package org.progressoft;

import liquibase.Contexts;
import liquibase.LabelExpression;
import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.LiquibaseException;
import liquibase.resource.ClassLoaderResourceAccessor;

import javax.sql.DataSource;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Objects;

public class BisDatabaseInitializer {

    public void initialize(DataSource dataSource) {
        if (Objects.isNull(dataSource)) {
            throw new NullPointerException("Null datasource");
        }
        Database database;
        try (Connection connection = dataSource.getConnection()) {
            database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(new JdbcConnection(connection));
            Path path = Paths.get("dbchangelog.xml");
            Liquibase liquibase = new Liquibase(path.toString(), new ClassLoaderResourceAccessor(), database);
            liquibase.update(new Contexts(), new LabelExpression());
        } catch (SQLException | LiquibaseException e) {
            throw new IllegalStateException(e);
        }
    }
}
