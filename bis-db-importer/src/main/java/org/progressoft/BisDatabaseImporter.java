package org.progressoft;

import org.progressoft.exception.ImportException;
import org.progressoft.interfaces.RateRecord;
import org.progressoft.interfaces.Rates;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Year;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

public class BisDatabaseImporter {

    private final DataSource dataSource;

    public BisDatabaseImporter(DataSource datasource) {
        if (Objects.isNull(datasource)) {
            throw new NullPointerException("Null datasource");
        }
        this.dataSource = datasource;
    }

    public void importRecord(RateRecord<?> record) {
        if (Objects.isNull(record)) {
            throw new NullPointerException("Null record");
        }
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement currencyInsertStatement = connection.prepareStatement("insert into bis_currency(curr_code, curr_name) values(?, ?)");
            PreparedStatement areaInsertStatement = connection.prepareStatement("INSERT INTO bis_area(area_code) VALUES(?)");
            PreparedStatement collectionTypeInsertStatement = connection.prepareStatement("INSERT INTO bis_collection_type(coll_code, coll_name) VALUES(?,?)");
            PreparedStatement currencyQuery = connection.prepareStatement("SELECT curr_id FROM bis_currency where curr_code = ?");
            PreparedStatement areaQuery = connection.prepareStatement("SELECT area_id FROM bis_area where area_code = ?");
            PreparedStatement collectionTypeQuery = connection.prepareStatement("SELECT coll_id FROM bis_collection_type WHERE coll_name = ?");
            PreparedStatement yearlyRatesInsertStatement = connection.prepareStatement("INSERT INTO bis_yearly_rates(curr_id, area_id, coll_id, year, rate) VALUES(?,?,?,?,?)");
            try (currencyInsertStatement;
                 currencyQuery;
                 areaInsertStatement;
                 areaQuery;
                 collectionTypeInsertStatement;
                 collectionTypeQuery;
                 yearlyRatesInsertStatement) {
                insertCurrency(record, currencyInsertStatement);
                insertArea(record, areaInsertStatement);
                insertCollectionType(record, collectionTypeInsertStatement);

                currencyQuery.setString(1, record.getCurrency().getCurrencyCode());
                areaQuery.setString(1, record.getAreaCode());
                collectionTypeQuery.setString(1, record.getCollectionType().toString());

                int currencyId = getCurrencyId(currencyQuery);
                int areaId = getAreaId(areaQuery);
                int collectionTypeId = getCollectionTypeId(collectionTypeQuery);
                insertYearlyRates(record, yearlyRatesInsertStatement, currencyId, areaId, collectionTypeId);
            }

        } catch (SQLException e) {
            throw new ImportException(e);
        }
    }

    private static int getCollectionTypeId(PreparedStatement collectionTypeQuery) throws SQLException {
        int collectionTypeId;
        ResultSet collectionResult = collectionTypeQuery.executeQuery();
        try (collectionResult) {
            if (!collectionResult.next()) {
                throw new ImportException("No collection record found");
            }
            collectionTypeId = collectionResult.getInt(1);
        }
        return collectionTypeId;
    }

    private static int getAreaId(PreparedStatement areaQuery) throws SQLException {
        int areaId;
        ResultSet areaResult = areaQuery.executeQuery();
        try (areaResult) {
            if (!areaResult.next()) {
                throw new ImportException("No area record found");
            }
            areaId = areaResult.getInt(1);
        }
        return areaId;
    }

    private static int getCurrencyId(PreparedStatement currencyQuery) throws SQLException {
        int currencyId;
        ResultSet currencyResult = currencyQuery.executeQuery();
        try (currencyResult) {
            if (!currencyResult.next()) {
                throw new ImportException("No currency record found");
            }
            currencyId = currencyResult.getInt(1);
        }
        return currencyId;
    }

    private static void insertYearlyRates(RateRecord<?> record, PreparedStatement yearlyRatesInsertStatement,
                                          int currencyId, int areaId, int collectionTypeId) throws SQLException {
        Rates<Year> rates = (Rates<Year>) record.getRates();
        Map<Year, BigDecimal> ratesMap = rates.getRatesMap();
        Set<Year> years = ratesMap.keySet();
        for (Year year : years) {
            yearlyRatesInsertStatement.setInt(1, currencyId);
            yearlyRatesInsertStatement.setInt(2, areaId);
            yearlyRatesInsertStatement.setInt(3, collectionTypeId);
            yearlyRatesInsertStatement.setInt(4, year.getValue());
            yearlyRatesInsertStatement.setBigDecimal(5, ratesMap.get(year));
            yearlyRatesInsertStatement.addBatch();
        }
        yearlyRatesInsertStatement.executeBatch();
    }

    private void insertCollectionType(RateRecord<?> record, PreparedStatement collectionTypeInsertStatement) throws SQLException {
        collectionTypeInsertStatement.setString(1, getCollectionTypeCode(record));
        collectionTypeInsertStatement.setString(2, record.getCollectionType().toString());
        collectionTypeInsertStatement.execute();
    }

    private String getCollectionTypeCode(RateRecord<?> record) {
        return record.getCollectionType() == CollectionType.AVERAGE ? "A" : "E";
    }

    private void insertArea(RateRecord<?> record, PreparedStatement areaInsertStatement) throws SQLException {
        areaInsertStatement.setString(1, record.getAreaCode());
        areaInsertStatement.execute();
    }

    private void insertCurrency(RateRecord<?> record, PreparedStatement currencyInsertStatement) throws SQLException {
        currencyInsertStatement.setString(1, record.getCurrency().getCurrencyCode());
        currencyInsertStatement.setString(2, record.getCurrency().getDisplayName());
        currencyInsertStatement.execute();
    }
}
