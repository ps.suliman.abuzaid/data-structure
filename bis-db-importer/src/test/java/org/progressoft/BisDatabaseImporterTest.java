package org.progressoft;

import org.junit.jupiter.api.Test;
import org.postgresql.ds.PGSimpleDataSource;
import org.progressoft.impl.CurrencyRecord;
import org.progressoft.interfaces.AnnualRates;
import org.progressoft.interfaces.RateRecord;

import javax.sql.DataSource;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Year;
import java.util.Currency;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.jupiter.api.Assertions.*;

public class BisDatabaseImporterTest {

    @Test
    void givenNullDatasource_whenConstructingBisDatabaseImporter_thenFail() {
        NullPointerException nullPointerException = assertThrows(NullPointerException.class,
                () -> new BisDatabaseImporter(null));
        assertEquals("Null datasource", nullPointerException.getMessage());
    }

    @Test
    void givenNullRecord_whenImportRecord_thenFail() {
        BisDatabaseImporter bisDatabaseImporter = getBisDatabaseImporter(getDataSource());
        NullPointerException nullPointerException = assertThrows(NullPointerException.class,
                () -> bisDatabaseImporter.importRecord(null));
        assertEquals("Null record", nullPointerException.getMessage());
    }

    @Test
    void givenAnnualAverageCurrencyRecord_whenImportRecord_thenSuccess() throws SQLException {
        CurrencyRecord currencyRecord = getCurrencyRecord(2023);

        DataSource dataSource = getDataSource();
        BisDatabaseInitializer databaseInitializer = new BisDatabaseInitializer();
        databaseInitializer.initialize(dataSource);
        AtomicInteger counter = new AtomicInteger();
        boolean[] closeFlag = new boolean[1];
        DataSource proxyDataSource = (DataSource) Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(),
                new Class[]{DataSource.class},
                (proxy, method, args) -> {
                    failIfNotGetConnection(method, args);
                    assertEquals(1, counter.incrementAndGet(), "getConnection was called more than once");
                    Connection connection = (Connection) method.invoke(dataSource, args);

                    return Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(),
                            connection.getClass().getInterfaces(),
                            (o, method1, objects) -> {
                                if (method1.getName().equals("close"))
                                    closeFlag[0] = true;
                                return method1.invoke(connection, objects);
                            });
                });

        BisDatabaseImporter bisDatabaseImporter = getBisDatabaseImporter(proxyDataSource);
        bisDatabaseImporter.importRecord(currencyRecord.getYearlyRateRecords().get(0));
        assertTrue(closeFlag[0], "closed flag is never called");

        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement currencyQuery = connection.prepareStatement("select curr_id, curr_code," +
                    "curr_name from bis_currency");
            PreparedStatement areaQuery = connection.prepareStatement("select area_id, area_code from bis_area");
            PreparedStatement collectionTypeQuery = connection.prepareStatement("select coll_id, coll_code, coll_name from bis_collection_type");
            PreparedStatement annualRatesQuery = connection.prepareStatement("select rate_id, curr_id, area_id, coll_id, year, rate from bis_yearly_rates");
            try (currencyQuery; areaQuery; collectionTypeQuery; annualRatesQuery) {
                ResultSet currencyResultSet = currencyQuery.executeQuery();
                ResultSet areaResultSet = areaQuery.executeQuery();
                ResultSet annualRatesResultSet = annualRatesQuery.executeQuery();
                ResultSet collectionTypeResultSet = collectionTypeQuery.executeQuery();
                try (currencyResultSet; areaResultSet; collectionTypeResultSet; annualRatesResultSet) {
                    assertCurrencyResult(currencyResultSet);
                    assertAreaResult(areaResultSet);
                    assertCollectionTypeResult(collectionTypeResultSet);
                    assertAnnualRatesResult(annualRatesResultSet);
                }
            }
        }

    }

    private static void failIfNotGetConnection(Method method, Object[] args) {
        if (!method.getName().equals("getConnection") || (args != null && args.length != 0)) {
            fail("Calling unNeeded method");
        }
    }

    private static void assertCollectionTypeResult(ResultSet collectionTypeResultSet) throws SQLException {
        assertTrue(collectionTypeResultSet.next(), "No collection type record found");
        assertEquals("A", collectionTypeResultSet.getString("coll_code"));
        assertEquals("AVERAGE", collectionTypeResultSet.getString("coll_name"));
        assertFalse(collectionTypeResultSet.next(), "More Records than expected are returned");
    }

    private static void assertAnnualRatesResult(ResultSet annualRatesResultSet) throws SQLException {
        assertTrue(annualRatesResultSet.next(), "No Annual records found");
        assertEquals("2023", annualRatesResultSet.getString("year"));
        assertEquals(new BigDecimal("0.7100000"), annualRatesResultSet.getBigDecimal("rate"));
        assertFalse(annualRatesResultSet.next(), "More records are returned");
    }

    private static void assertAreaResult(ResultSet areaResultSet) throws SQLException {
        assertTrue(areaResultSet.next(), "No Area Record found");
        assertEquals("JO", areaResultSet.getString("area_code"));
        assertFalse(areaResultSet.next(), "More records are returned");
    }

    private static void assertCurrencyResult(ResultSet currencyResultSet) throws SQLException {
        assertTrue(currencyResultSet.next(), "No Currency Record found");
        assertEquals("JOD", currencyResultSet.getString("curr_code"));
        assertFalse(currencyResultSet.next(), "More records are returned");
    }

    private static CurrencyRecord getCurrencyRecord(int year) {
        AnnualRates rates = getAnnualRates(year);

        RateRecord<Year> rateRecord = new RateRecord<>();
        rateRecord.setFrequency(Frequency.ANNUAL);
        rateRecord.setAreaCode("JO");
        rateRecord.setCurrency(Currency.getInstance("JOD"));
        rateRecord.setCollectionType(CollectionType.AVERAGE);
        rateRecord.setRates(rates);
        return new CurrencyRecord(rateRecord);
    }

    private static AnnualRates getAnnualRates(int year) {
        return new AnnualRates() {
            @Override
            public BigDecimal getRate(Year dateKey) {
                return null;
            }

            @Override
            public Map<Year, BigDecimal> getRatesMap() {
                return new HashMap<>() {{
                    String rate = null;
                    switch (year) {
                        case 2022:
                            rate = "0.70";
                            break;
                        case 2023:
                            rate = "0.71";
                            break;
                        default:
                            fail("Year does not have a rate");
                    }
                    put(Year.of(2023), new BigDecimal(rate));
                }};
            }
        };
    }

    private BisDatabaseImporter getBisDatabaseImporter(DataSource dataSource) {
        return new BisDatabaseImporter(dataSource);
    }

    private DataSource getDataSource() {
        PGSimpleDataSource datasource = new PGSimpleDataSource();
        datasource.setUrl("jdbc:postgresql://localhost:5432/bis_test");
        datasource.setUser("postgres");
        datasource.setPassword("postgres");
        return datasource;
    }
}
