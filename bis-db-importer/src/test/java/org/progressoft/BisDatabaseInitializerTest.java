package org.progressoft;

import org.junit.jupiter.api.Test;
import org.postgresql.ds.PGSimpleDataSource;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.*;

public class BisDatabaseInitializerTest {

    @Test
    void canCreate() {
        new BisDatabaseInitializer();
    }

    @Test
    void givenNullDatasource_whenInitialize_thenFail() {
        BisDatabaseInitializer bisDatabaseInitializer = new BisDatabaseInitializer();
        NullPointerException nullPointerException = assertThrows(NullPointerException.class,
                () -> bisDatabaseInitializer.initialize(null));
        assertEquals("Null datasource", nullPointerException.getMessage());
    }

    @Test
    void givenValidDatasource_whenInitialize_thenSuccess() throws SQLException {
        PGSimpleDataSource datasource = new PGSimpleDataSource();
        datasource.setUrl("jdbc:postgresql://localhost:5432/bis_test");
        datasource.setUser("postgres");
        datasource.setPassword("postgres");

        BisDatabaseInitializer databaseInitializer = new BisDatabaseInitializer();
        databaseInitializer.initialize(datasource);
        Connection connection = datasource.getConnection();
        try (connection) {
            DatabaseMetaData databaseMetaData = connection.getMetaData();
            ResultSet tables = databaseMetaData.getTables(null, null, "bis%", null);
            try (tables) {
                assertTrue(tables.next());
            }
        }
    }
}
