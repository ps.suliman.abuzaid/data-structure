package org.progressoft.finance;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Currency;

import static org.junit.jupiter.api.Assertions.*;


class DefaultRateSupplierTest {

    private final RateSupplier rateSupplier = new DefaultRateSupplier();


    @Test
    void givenTwoValidCurrenciesWithNoRate_whenGetRate_thenThrowIllegalStateException() {
        Currency fromCurrency = Currency.getInstance("EUR");
        Currency toCurrency = Currency.getInstance("USD");
        IllegalStateException illegalStateException = assertThrows(IllegalStateException.class,
                () -> rateSupplier.getRate(fromCurrency, toCurrency));
        assertEquals("No Rate found for these currencies", illegalStateException.getMessage());
    }

    @Test
    void givenTwoValidCurrencies_whenGetRate_thenReturnCurrenciesExchangeRate() {
        Currency fromCurrency = Currency.getInstance("JOD");
        Currency toCurrency = Currency.getInstance("USD");
        BigDecimal expectedRate = BigDecimal.valueOf(1.41);
        BigDecimal actualRate = rateSupplier.getRate(fromCurrency, toCurrency);
        assertNotNull(actualRate, "Rate should not be null");
        assertEquals(expectedRate, actualRate);
        assertEquals(expectedRate.scale(), actualRate.scale());
    }
}