package org.progressoft.finance;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Currency;

class DefaultCurrencyValidatorTest {

    private final CurrencyValidator currencyValidator = new DefaultCurrencyValidator();

    @Test
    void givenNullCurrency_whenIsValid_thenReturnFalse() {
        Assertions.assertFalse(currencyValidator.isValid(null));
    }

    @Test
    void givenCurrency_whenIsValid_thenReturnTrue() {
        Assertions.assertTrue(currencyValidator.isValid(Currency.getInstance("JOD")));
    }

}