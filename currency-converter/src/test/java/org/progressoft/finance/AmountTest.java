package org.progressoft.finance;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Currency;

import static org.junit.jupiter.api.Assertions.*;

class AmountTest {

    @Test
    void givenNullValueCurrencyAmount_whenConstructingAmount_thenThrowIllegalArgumentException() {
        IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class,
                () -> new Amount(Currency.getInstance("JOD"), null));
        assertEquals("amount must not be null", illegalArgumentException.getMessage());
    }

    @Test
    void givenNegativeAmount_whenConstructingAmount_thenThrowIllegalArgumentException() {
        IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class,
                () -> new Amount(Currency.getInstance("JOD"), BigDecimal.valueOf(-10)));
        assertEquals("amount must positive", illegalArgumentException.getMessage());
    }

    @Test
    void givenNullFromCurrencyInSourceAmountInput_whenConvert_thenThrowIllegalArgumentException() {
        BigDecimal amountValue = BigDecimal.valueOf(15);
        IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class,
                () -> new Amount(null, amountValue));
        assertEquals("sourceCurrency must not be null", illegalArgumentException.getMessage());
    }


    @Test
    void givenHighAmount_whenConstructingAmount_thenThrowIllegalArgumentException() {
        IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class,
                () -> new Amount(Currency.getInstance("JOD"), new BigDecimal("1.2323242323")));
        assertEquals("amount scale must be equal to currency fractions", illegalArgumentException.getMessage());
    }

    @Test
    void givenValidInput_whenConstructingAmount_thenReturnNewAmount() {
        Amount sourceAmount = new Amount(Currency.getInstance("JOD"),
                BigDecimal.valueOf(5.789));

        assertNotNull(sourceAmount, "amount must not be null");
        assertEquals("JOD", sourceAmount.getFromCurrency().getCurrencyCode());
        assertEquals(BigDecimal.valueOf(5.789), sourceAmount.getValue());
    }
}