package org.progressoft.finance;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Currency;

import static org.junit.jupiter.api.Assertions.*;

class ReverseRateSupplierTest {

    private final RateSupplier rateSupplier = new ReverseRateSupplier(new DefaultRateSupplier());;

    @Test
    void givenTwoValidReversedCurrencies_whenGetRate_thenReturnExchangeRate() {
        Currency fromCurrency = Currency.getInstance("USD");
        Currency toCurrency = Currency.getInstance("JOD");
        BigDecimal expectedRate = BigDecimal.valueOf(0.709).setScale(3, RoundingMode.HALF_EVEN);
        BigDecimal actualRate = rateSupplier.getRate(fromCurrency, toCurrency);

        assertNotNull(actualRate, "rate must not be null");
        assertEquals(expectedRate, actualRate);
    }

    @Test
    void givenTwoValidCurrenciesWithNoRate_whenGetRate_thenThrowIllegalStateException() {
        Currency fromCurrency = Currency.getInstance("USD");
        Currency toCurrency = Currency.getInstance("EUR");
        IllegalStateException illegalStateException = assertThrows(IllegalStateException.class, () -> rateSupplier.getRate(fromCurrency, toCurrency));
        assertEquals("No Rate found for these currencies", illegalStateException.getMessage());
    }

}