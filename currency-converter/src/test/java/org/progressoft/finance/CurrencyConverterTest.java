package org.progressoft.finance;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Currency;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class CurrencyConverterTest {

    private final CurrencyConverter currencyConverter = new DefaultCurrencyConverter();
    
    @Test
    void givenNullTargetCurrencyInput_whenConvert_thenThrowIllegalArgumentException() {
        Currency sourceCurrency = Currency.getInstance("JOD");
        BigDecimal amountValue = BigDecimal.valueOf(15).setScale(sourceCurrency.getDefaultFractionDigits(), RoundingMode.HALF_EVEN);
        Amount amount = new Amount(sourceCurrency, amountValue);
        IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class,
                () -> currencyConverter.convert(new CurrencyConverterRequest(amount, null)));
        assertEquals("targetCurrency must not be null", illegalArgumentException.getMessage());
    }

    @Test
    void givenNullSourceAmountInput_whenConvert_thenThrowIllegalArgumentException() {
        Currency targetCurrency = Currency.getInstance("USD");
        IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class, () -> currencyConverter.convert(new CurrencyConverterRequest(null, targetCurrency)));
        assertEquals("sourceAmount must not be null", illegalArgumentException.getMessage());
    }

    @Test
    void givenValidRequest_whenConvert_thenReturnResult() {
        Currency fromCurrency = Currency.getInstance("JOD");
        Currency toCurrency = Currency.getInstance("USD");
        Amount amount = new Amount(fromCurrency,
                BigDecimal.valueOf(15).setScale(fromCurrency.getDefaultFractionDigits(), RoundingMode.HALF_EVEN));
        BigDecimal expectedResult = new BigDecimal("21.15");
        BigDecimal actualResult = currencyConverter.convert(new CurrencyConverterRequest(amount, toCurrency));
        assertEquals(expectedResult, actualResult);

    }

    @Test
    void givenValidRequestForHighAmount_whenConvert_thenExpected() {
        Currency fromCurrency = Currency.getInstance("JOD");
        Currency toCurrency = Currency.getInstance("USD");
        Amount amount = new Amount(fromCurrency, new BigDecimal("13.698"));
        BigDecimal actual = currencyConverter.convert(new CurrencyConverterRequest(amount, toCurrency));
        assertEquals(BigDecimal.valueOf(19.31), actual);
    }
}
