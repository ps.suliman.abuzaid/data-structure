package org.progressoft.finance;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class DefaultCurrencyConverter implements CurrencyConverter {

    private final RateSupplier rateSupplier;
    private final CurrencyValidator converterValidator;

    DefaultCurrencyConverter() {
        this(new DefaultRateSupplier(), new DefaultCurrencyValidator());
    }

    DefaultCurrencyConverter(RateSupplier rateSupplier, CurrencyValidator converterValidator) {
        this.rateSupplier = rateSupplier;
        this.converterValidator = converterValidator;
    }

    @Override
    public BigDecimal convert(CurrencyConverterRequest currencyConverterRequest) {
        if (currencyConverterRequest.getSourceAmount() == null)
            throw new IllegalArgumentException("sourceAmount must not be null");
        if (!converterValidator.isValid(currencyConverterRequest.getTargetCurrency()))
            throw new IllegalArgumentException("targetCurrency must not be null");
        BigDecimal rate = rateSupplier.getRate(currencyConverterRequest.getSourceAmount().getFromCurrency(), currencyConverterRequest.getTargetCurrency());
        return currencyConverterRequest.getSourceAmount().getValue().multiply(rate)
                .setScale(currencyConverterRequest.getTargetCurrency().getDefaultFractionDigits(), RoundingMode.HALF_EVEN);
    }
}
