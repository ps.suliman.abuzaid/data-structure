package org.progressoft.finance;

import java.util.Currency;
import java.util.Objects;

public class DefaultCurrencyValidator implements CurrencyValidator {
    @Override
    public boolean isValid(Currency currency) {
        return !Objects.isNull(currency);
    }
}
