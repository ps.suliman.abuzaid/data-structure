package org.progressoft.finance;

import java.math.BigDecimal;

public interface CurrencyConverter {
    BigDecimal convert(CurrencyConverterRequest currencyConverterRequest);
}
