package org.progressoft.finance;

import java.math.BigDecimal;
import java.util.Currency;

public interface RateSupplier {
    BigDecimal getRate(Currency fromCurrency, Currency toCurrency);
}
