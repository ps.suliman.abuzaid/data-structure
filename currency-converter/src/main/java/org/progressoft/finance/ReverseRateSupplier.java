package org.progressoft.finance;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Currency;

public class ReverseRateSupplier implements RateSupplier {
    private final RateSupplier rateSupplier;

    public ReverseRateSupplier(RateSupplier rateSupplier) {
        this.rateSupplier = rateSupplier;
    }

    @Override
    public BigDecimal getRate(Currency fromCurrency, Currency toCurrency) {
        BigDecimal rate;
        try {
            rate = rateSupplier.getRate(fromCurrency, toCurrency);
        } catch (IllegalStateException illegalStateException) {
            rate = BigDecimal.ONE.divide(rateSupplier.getRate(toCurrency, fromCurrency), 3, RoundingMode.HALF_EVEN);
        }
        return rate;
    }
}