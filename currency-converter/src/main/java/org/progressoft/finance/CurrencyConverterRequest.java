package org.progressoft.finance;

import java.util.Currency;

public class CurrencyConverterRequest {
    private final Amount sourceAmount;

    private final Currency targetCurrency;

    public CurrencyConverterRequest(Amount sourceAmount, Currency targetCurrency) {
        this.sourceAmount = sourceAmount;
        this.targetCurrency = targetCurrency;
    }

    public Amount getSourceAmount() {
        return sourceAmount;
    }

    public Currency getTargetCurrency() {
        return targetCurrency;
    }

}
