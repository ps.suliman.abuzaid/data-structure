package org.progressoft.finance;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Objects;
import java.util.function.Predicate;

//TODO Rename to Amount
public class Amount {

    private final Currency fromCurrency;
    private final BigDecimal value;


    public Amount(Currency fromCurrency, BigDecimal value) {
        throwIfTrue(value, Objects::isNull, "amount must not be null");
        throwIfTrue(value, amount -> amount.signum() < 0, "amount must positive");
        if (Objects.isNull(fromCurrency)) {
            throw new IllegalArgumentException("sourceCurrency must not be null");
        }
        throwIfTrue(value, amount -> amount.scale() != fromCurrency.getDefaultFractionDigits(), "amount scale must be equal to currency fractions");

        this.fromCurrency = fromCurrency;
        this.value = value;
    }

    public BigDecimal getValue() {
        return value;
    }

    public Currency getFromCurrency() {
        return fromCurrency;
    }

    private void throwIfTrue(BigDecimal amount, Predicate<BigDecimal> testCondition, String errorMessage) {
        if (testCondition.test(amount)) {
            throw new IllegalArgumentException(errorMessage);
        }
    }
}
