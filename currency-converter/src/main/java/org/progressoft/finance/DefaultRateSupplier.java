package org.progressoft.finance;

import java.math.BigDecimal;
import java.util.Currency;

public class DefaultRateSupplier implements RateSupplier {
    @Override
    public BigDecimal getRate(Currency fromCurrency, Currency toCurrency) {
        if (fromCurrency.getCurrencyCode().equals("JOD")
                && toCurrency.getCurrencyCode().equals("USD"))
            return BigDecimal.valueOf(1.41);
        throw new IllegalStateException("No Rate found for these currencies");
    }
}
