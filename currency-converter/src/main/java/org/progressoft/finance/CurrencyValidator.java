package org.progressoft.finance;

import java.util.Currency;

public interface CurrencyValidator {
    boolean isValid(Currency currency);
}
