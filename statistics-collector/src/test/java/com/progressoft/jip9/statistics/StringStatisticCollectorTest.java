package com.progressoft.jip9.statistics;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

public class StringStatisticCollectorTest {
    private StatisticsCollector<String, ? extends Statistic> collector;

    @BeforeEach
    void setup() {
        List<StatisticMetricCondition<Character>> dummyStatisticMetricConditions = new ArrayList<>();
        collector = new StringStatisticsCollector(dummyStatisticMetricConditions);
    }

    @Test
    void givenNullInput_whenCollectStatistics_thenThrowIllegalArgumentException() {
        IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class,
                () -> collector.collectStatistics(null));
        assertEquals("Iterable of Objects must not be null", illegalArgumentException.getMessage());
    }

    @Test
    void givenEmptyInput_whenCollectStatistics_thenThrowIllegalArgumentException() {
        IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class,
                () -> collector.collectStatistics(new ArrayList<>()));
        assertEquals("Iterable of Objects must not be empty", illegalArgumentException.getMessage());
    }

    @Test
    public void givenStringCases_whenCollectStatistics_thenStatisticsReturnedAsExpected() {
        List<String> cases = prepareCases();
        assertNotNull(collector, "you should initialize your collector");
        Iterable<? extends Statistic> statistics = collector.collectStatistics(cases);
        assertNotNull(statistics, "returned statistics is null");

        Map<String, Object> mapping = new HashMap<>();
        statistics.forEach(s -> mapping.put(s.getKey(), s.getValue()));
        assertFalse(mapping.isEmpty(), "no statistics was returned");
        Map<String, Object> expected = getExpectedResultMap();
        assertEquals(expected, mapping, "returned statistics are not within expected");
    }

    private static Map<String, Object> getExpectedResultMap() {
        Map<String, Object> expected = new HashMap<>();
        expected.put("upper case", 3);
        expected.put("lower case", 37);
        expected.put("non-word", 4);
        expected.put("total words", 8);
        expected.put("digits", 3);
        return expected;
    }

    private List<String> prepareCases() {
        return Arrays.asList("hello world",
                "Hakona Matata!",
                "my password P@ssw0rd",
                "23%&kl");
    }
}
