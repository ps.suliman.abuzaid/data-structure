package com.progressoft.jip9.statistics;

import com.progressoft.jip9.entity.Employee;
import com.progressoft.jip9.statistics.Statistic;
import com.progressoft.jip9.statistics.StatisticsCollector;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class EmployeeStatisticCollectorTest {
    private StatisticsCollector<Employee, ? extends Statistic> collector;

    @BeforeEach
    void setup() {
        collector = new EmployeeStatisticCollector();
    }

    @Test
    void givenNullInput_whenCollectStatistics_thenThrowIllegalArgumentException() {
        IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class,
                () -> collector.collectStatistics(null));
        assertEquals("Iterable of Objects must not be null", illegalArgumentException.getMessage());
    }

    @Test
    void givenEmptyInput_whenCollectStatistics_thenThrowIllegalArgumentException() {
        IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class,
                () -> collector.collectStatistics(new ArrayList<>()));
        assertEquals("Iterable of Objects must not be empty", illegalArgumentException.getMessage());
    }

    @Test
    public void givenEmployees_whenCollectStatistics_thenStatisticsReturnedAsExpected() {
        List<Employee> cases = prepareCases();
        Assertions.assertNotNull(collector, "you should initialize your collector");
        Iterable<? extends Statistic> statistics = collector.collectStatistics(cases);
        Assertions.assertNotNull(statistics, "returned statistics is null");

        Map<String, Object> mapping = new HashMap<>();
        statistics.forEach(s -> mapping.put(s.getKey(), s.getValue()));
        Assertions.assertFalse(mapping.isEmpty(), "no statistics was returned");
        Map<String, Object> expected = getExpectedResultMap();

        Assertions.assertEquals(expected, mapping, "returned statistics are not within expected");
    }

    private static Map<String, Object> getExpectedResultMap() {
        Map<String, Object> expected = new HashMap<>();

        expected.put("born in 1997", 2);
        expected.put("born in 1995", 3);

        expected.put("born in Jordan", 2);
        expected.put("born in Palestine", 1);
        expected.put("born in egypt", 2);

        expected.put("salary < 350", 1);
        expected.put("350 <= salary < 600", 1);
        expected.put("600 <= salary < 1,200", 2);
        expected.put("salary >= 1,200", 1);

        expected.put("resigned in 2018", 2);
        expected.put("resigned in 2019", 1);

        expected.put("developer", 3);
        expected.put("project manager", 1);
        expected.put("quality control", 1);
        return expected;
    }

    private List<Employee> prepareCases() {
        return Arrays.asList(new Employee.Builder()
                        .setFirstName("sami")
                        .setLastName("ahmad")
                        .setBirthDate(LocalDate.of(1995, 3, 1))
                        .setBirthPlace("Jordan")
                        .setSalary(BigDecimal.valueOf(340))
                        .setResignationDate(LocalDate.of(2018, 10, 10))
                        .setPosition("developer")
                        .build(),
                new Employee.Builder()
                        .setFirstName("Moustafa")
                        .setLastName("Ismail")
                        .setBirthDate(LocalDate.of(1995, 4, 11))
                        .setBirthPlace("Palestine")
                        .setSalary(BigDecimal.valueOf(400))
                        .setPosition("developer")
                        .build(),
                new Employee.Builder()
                        .setFirstName("Abdellrahma")
                        .setLastName("Saed")
                        .setBirthDate(LocalDate.of(1995, 5, 1))
                        .setBirthPlace("Jordan")
                        .setSalary(BigDecimal.valueOf(700))
                        .setResignationDate(LocalDate.of(2018, 11, 10))
                        .setPosition("developer")
                        .build(),
                new Employee.Builder()
                        .setFirstName("Hussam")
                        .setLastName("Husni")
                        .setBirthDate(LocalDate.of(1997, 5, 1))
                        .setBirthPlace("egypt")
                        .setSalary(BigDecimal.valueOf(1_000))
                        .setPosition("quality control")
                        .build(),
                new Employee.Builder()
                        .setFirstName("Mohammad")
                        .setLastName("Abdelsalam")
                        .setBirthDate(LocalDate.of(1997, 10, 15))
                        .setSalary(BigDecimal.valueOf(1_430))
                        .setBirthPlace("egypt")
                        .setResignationDate(LocalDate.of(2019, 10, 10))
                        .setPosition("project manager")
                        .build());

    }
}
