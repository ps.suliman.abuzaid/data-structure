package com.progressoft.jip9.entity;

import java.math.BigDecimal;
import java.time.LocalDate;

public class Employee {

    private final String firstName;
    private final String lastName;
    private final String birthPlace;
    private final LocalDate birthDate;
    private final LocalDate hiringDate;
    private LocalDate resignationDate;
    private final String position;
    private BigDecimal salary;

    private Employee(Builder builder) {
        this.firstName = builder.firstName;
        this.lastName = builder.lastName;
        this.birthPlace = builder.birthPlace;
        this.birthDate = builder.birthDate;
        this.hiringDate = builder.hiringDate;
        this.resignationDate = builder.resignationDate;
        this.position = builder.position;
        this.salary = builder.salary;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public LocalDate getHiringDate() {
        return hiringDate;
    }

    public LocalDate getResignationDate() {
        return resignationDate;
    }

    public String getPosition() {
        return position;
    }

    public BigDecimal getSalary() {
        return salary;
    }

    public void setResignationDate(LocalDate resignationDate) {
        this.resignationDate = resignationDate;
    }

    public void setSalary(BigDecimal salary) {
        this.salary = salary;
    }

    public static class Builder {

        private String firstName;
        private String lastName;
        private String birthPlace;
        private LocalDate birthDate;
        private LocalDate hiringDate;
        private LocalDate resignationDate;
        private String position;
        private BigDecimal salary;

        public Builder setFirstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public Builder setLastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public Builder setBirthPlace(String birthPlace) {
            this.birthPlace = birthPlace;
            return this;
        }

        public Builder setBirthDate(LocalDate birthDate) {
            this.birthDate = birthDate;
            return this;
        }

        public Builder setHiringDate(LocalDate hiringDate) {
            this.hiringDate = hiringDate;
            return this;
        }

        public Builder setResignationDate(LocalDate resignationDate) {
            this.resignationDate = resignationDate;
            return this;
        }

        public Builder setPosition(String position) {
            this.position = position;
            return this;
        }

        public Builder setSalary(BigDecimal salary) {
            this.salary = salary;
            return this;
        }

        public Employee build() {
            return new Employee(this);
        }
    }
}
