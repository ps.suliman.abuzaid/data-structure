package com.progressoft.jip9.statistics;

import com.progressoft.jip9.entity.Employee;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class EmployeeStatisticCollector implements StatisticsCollector<Employee, Statistic> {
    private final Map<String, Integer> metricsData;

    EmployeeStatisticCollector() {
        metricsData = new HashMap<>();
    }

    @Override
    public Iterable<Statistic> collectStatistics(Iterable<Employee> objects) {
        if (Objects.isNull(objects)) {
            throw new IllegalArgumentException("Iterable of Objects must not be null");
        }

        if (!objects.iterator().hasNext()) {
            throw new IllegalArgumentException("Iterable of Objects must not be empty");
        }

        for (Employee employee : objects) {
            countKey("born in " + employee.getBirthDate().getYear());
            countKey("born in " + employee.getBirthPlace());

            if (employee.getSalary().compareTo(BigDecimal.valueOf(350)) < 0) {
                countKey("salary < 350");
            } else if (employee.getSalary().compareTo(BigDecimal.valueOf(350)) >= 0
                    && employee.getSalary().compareTo(BigDecimal.valueOf(600)) < 0) {
                countKey("350 <= salary < 600");
            } else if (employee.getSalary().compareTo(BigDecimal.valueOf(600)) >= 0
                    && employee.getSalary().compareTo(BigDecimal.valueOf(1200)) < 0) {
                countKey("600 <= salary < 1,200");
            } else {
                countKey("salary >= 1,200");
            }

            if (employee.getResignationDate() != null) {
                countKey("resigned in " + employee.getResignationDate().getYear());
            }
            countKey(employee.getPosition());
        }

        return metricsData.entrySet().stream().map((metricData) -> new StatisticMetric(metricData.getKey(), metricData.getValue())).collect(Collectors.toList());
    }

    private void countKey(String key) {
        metricsData.compute(key, (k, v) -> (v == null) ? 1 : v + 1);
    }
}
