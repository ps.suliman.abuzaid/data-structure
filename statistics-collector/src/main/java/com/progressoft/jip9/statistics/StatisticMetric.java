package com.progressoft.jip9.statistics;

public class StatisticMetric implements Statistic {
    private final String key;
    private final Integer value;

    public StatisticMetric(String key, Integer value) {
        this.key = key;
        this.value = value;
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public Integer getValue() {
        return value;
    }
}
