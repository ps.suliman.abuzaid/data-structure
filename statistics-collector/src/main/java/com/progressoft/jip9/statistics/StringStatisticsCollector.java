package com.progressoft.jip9.statistics;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class StringStatisticsCollector implements StatisticsCollector<String, Statistic> {

    private final StringBuilder allWords;
    private final List<StatisticMetricCondition<Character>> statisticMetricConditions;

    public StringStatisticsCollector() {
        this.allWords = new StringBuilder();
        statisticMetricConditions = new ArrayList<>();
        statisticMetricConditions.add(new StatisticMetricConditionImpl<>("upper case", 0, Character::isUpperCase));
        statisticMetricConditions.add(new StatisticMetricConditionImpl<>("lower case", 0, Character::isLowerCase));
        statisticMetricConditions.add(new StatisticMetricConditionImpl<>("non-word", 0,
                character -> character != ' ' && !Character.isLetter(character) && !Character.isDigit(character)));
        statisticMetricConditions.add(new StatisticMetricConditionImpl<>("total words", 0, character -> character == ' '));
        statisticMetricConditions.add(new StatisticMetricConditionImpl<>("digits", 0, Character::isDigit));
    }

    public StringStatisticsCollector(List<StatisticMetricCondition<Character>> statisticMetricConditions) {
        this();
        this.statisticMetricConditions.addAll(statisticMetricConditions);
    }

    @Override
    public Iterable<Statistic> collectStatistics(Iterable<String> objects) {
        if (Objects.isNull(objects)) {
            throw new IllegalArgumentException("Iterable of Objects must not be null");
        }

        if (!objects.iterator().hasNext()) {
            throw new IllegalArgumentException("Iterable of Objects must not be empty");
        }

        appendWords(objects);

        for (int index = 0; index < allWords.length(); index++) {
            char character = allWords.charAt(index);
            for (StatisticMetricCondition<Character> metric : statisticMetricConditions)
                if (metric.testCondition(character))
                    break;
        }

        return statisticMetricConditions.stream().map(statisticMetricCondition -> new StatisticMetric(statisticMetricCondition.getKey(), statisticMetricCondition.getValue())).collect(Collectors.toList());
    }

    private void appendWords(Iterable<String> objects) {
        for (String object : objects) {

            allWords.append(removeExtraSpaces(object));
            allWords.append(" ");
        }

    }

    public String removeExtraSpaces(String inputString) {
        return inputString.replaceAll("\\s+", " ").trim();
    }
}
