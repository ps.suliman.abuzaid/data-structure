package com.progressoft.jip9.statistics;

import java.util.function.Predicate;

public class StatisticMetricConditionImpl<T> implements StatisticMetricCondition<T> {
    private final String key;

    private Integer value;
    private final Predicate<T> condition;

    public StatisticMetricConditionImpl(String key, Integer value, Predicate<T> condition) {
        this.key = key;
        this.value = value;
        this.condition = condition;
    }

    @Override
    public boolean testCondition(T testValue) {
        if (condition.test(testValue)) {
            value++;
            return true;
        }
        return false;
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public Integer getValue() {
        return value;
    }
}
