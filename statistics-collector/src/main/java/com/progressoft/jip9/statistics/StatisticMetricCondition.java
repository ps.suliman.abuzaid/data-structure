package com.progressoft.jip9.statistics;

public interface StatisticMetricCondition<T> {
    boolean testCondition(T testValue);

    String getKey();

    Integer getValue();
}
