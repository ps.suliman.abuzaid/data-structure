package org.progressoft;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.progressoft.finance.Amount;
import org.progressoft.finance.CurrencyConverter;
import org.progressoft.finance.CurrencyConverterRequest;
import org.progressoft.finance.RateSupplier;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Currency;

import static org.junit.jupiter.api.Assertions.*;

public class BisCurrencyConverterTest {
    private final CurrencyConverter currencyConverter;
    public BisCurrencyConverterTest() throws IOException {
        Path path = Files.createTempFile("bis", ".csv");
        try (InputStream resourceAsStream = this.getClass().getResourceAsStream("/ExchangeRates.csv");
             OutputStream outputStream = Files.newOutputStream(path)) {
            int read;
            while (resourceAsStream != null && (read = resourceAsStream.read()) != -1){
                outputStream.write(read);
            }
        }
        BisFileDataFetcher bisFileDataFetcher = new BisFileDataFetcher(path);
        RateSupplier supplier = new BisRateSupplier(bisFileDataFetcher);
        currencyConverter = new BisCurrencyConverter(supplier);
    }

    @Test
    void givenNullConverterRequest_whenConvert_thenThrowIllegalArgumentException(){
        IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class,
                () -> currencyConverter.convert(null));
        assertEquals("Currency Request must not be null",illegalArgumentException.getMessage());
    }

    @Test
    void givenValidInput_whenConvert_thenReturnResult(){
        Amount amount = new Amount(Currency.getInstance("USD"), new BigDecimal("100.00"));
        BigDecimal result = currencyConverter.convert(new CurrencyConverterRequest(amount, Currency.getInstance("JOD")));
        assertNotNull(result);
        assertEquals(new BigDecimal("71.000"), result);
    }
}
