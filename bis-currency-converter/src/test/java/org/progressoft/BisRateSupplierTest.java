package org.progressoft;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.progressoft.finance.RateSupplier;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Currency;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BisRateSupplierTest {

    private final RateSupplier supplier;

    public BisRateSupplierTest() throws IOException {
        Path path = Files.createTempFile("bis", ".csv");
        try (InputStream resourceAsStream = this.getClass().getResourceAsStream("/ExchangeRates.csv");
             OutputStream outputStream = Files.newOutputStream(path)) {
            int read;
            while (resourceAsStream != null && (read = resourceAsStream.read()) != -1){
                outputStream.write(read);
            }
        }
        BisFileDataFetcher bisFileDataFetcher = new BisFileDataFetcher(path);
        supplier = new BisRateSupplier(bisFileDataFetcher);
    }

    @Test
    void givenValidInput_whenGetRate_thenReturnExchangeRate(){
        Currency sourceCurrency = Currency.getInstance("JOD");
        Currency usdTargetCurrency = Currency.getInstance("USD");
        Currency eurTargetCurrency = Currency.getInstance("EUR");
        BigDecimal expectedJodToUsdRate = new BigDecimal("1.4084507");
        BigDecimal expectedJodToEurRate = new BigDecimal("1.2537394");
        BigDecimal actualJodToUsdRate = supplier.getRate(sourceCurrency, usdTargetCurrency);
        BigDecimal actualJodToEurRate = supplier.getRate(sourceCurrency, eurTargetCurrency);
        assertEquals(expectedJodToUsdRate, actualJodToUsdRate);
        assertEquals(expectedJodToEurRate, actualJodToEurRate);
    }
}
