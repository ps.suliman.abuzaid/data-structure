package org.progressoft;

import org.progressoft.finance.CurrencyConverter;
import org.progressoft.finance.CurrencyConverterRequest;
import org.progressoft.finance.RateSupplier;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Objects;

public class BisCurrencyConverter implements CurrencyConverter {

    private final RateSupplier rateSupplier;

    public BisCurrencyConverter(RateSupplier rateSupplier) {
        this.rateSupplier = rateSupplier;
    }

    @Override
    public BigDecimal convert(CurrencyConverterRequest currencyConverterRequest) {
        if (Objects.isNull(currencyConverterRequest)) {
            throw new IllegalArgumentException("Currency Request must not be null");
        }
        BigDecimal rate = rateSupplier.getRate(currencyConverterRequest.getSourceAmount().getFromCurrency(),
                currencyConverterRequest.getTargetCurrency());

        return currencyConverterRequest.getSourceAmount().getValue().multiply(rate).
                setScale(currencyConverterRequest.getTargetCurrency().getDefaultFractionDigits(),
                        RoundingMode.HALF_EVEN);
    }
}
