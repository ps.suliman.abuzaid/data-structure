package org.progressoft;

import org.progressoft.finance.RateSupplier;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.Path;
import java.util.Currency;
import java.util.Map;

public class BisRateSupplier implements RateSupplier {

    private final BisFileDataFetcher bisFileDataFetcher;

    public BisRateSupplier(BisFileDataFetcher bisFileDataFetcher) {
        this.bisFileDataFetcher = bisFileDataFetcher;
    }

    @Override
    public BigDecimal getRate(Currency sourceCurrency, Currency targetCurrency) {
        BigDecimal usdToSourceCurrency = bisFileDataFetcher.getCurrencyRateFromBase(sourceCurrency);
        BigDecimal usdToTargetCurrencyRate = bisFileDataFetcher.getCurrencyRateFromBase(targetCurrency);
        return usdToTargetCurrencyRate.divide(usdToSourceCurrency, 7, RoundingMode.HALF_EVEN);
    }
}
