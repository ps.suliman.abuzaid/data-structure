package org.progressoft;

import java.util.NoSuchElementException;

public enum CollectionType {
    AVERAGE, EOP;

    public static CollectionType byCode(String collectionTypeCode){
        switch (collectionTypeCode){
            case "A":
                return AVERAGE;
            case "E":
                return EOP;
        }
        throw new NoSuchElementException("No Collection type for this code");
    }
}