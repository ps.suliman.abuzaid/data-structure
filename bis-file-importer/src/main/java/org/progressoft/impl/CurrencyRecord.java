package org.progressoft.impl;

import org.progressoft.interfaces.BaseRecord;

import java.util.Currency;
import java.util.Objects;

public class CurrencyRecord extends BaseRecord<Currency, CountryRecord> {

    public CurrencyRecord(FileMetaData fileMetaData) {
        String[] record = fileMetaData.getRecords();
        Currency currency = Currency.getInstance(record[4]);
        addRecord(currency, new CountryRecord(fileMetaData));
    }

    @Override
    public void passFileData(FileMetaData fileMetaData) {
        String[] record = fileMetaData.getRecords();
        Currency currency = Currency.getInstance(record[4]);
        CountryRecord countryRecord = getRecord(currency);
        if (Objects.isNull(countryRecord)) {
            addRecord(currency, new CountryRecord(fileMetaData));
            return;
        }
        countryRecord.passFileData(fileMetaData);

    }
}
