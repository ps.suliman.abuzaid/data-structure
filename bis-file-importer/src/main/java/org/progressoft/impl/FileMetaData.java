package org.progressoft.impl;

import java.util.Arrays;

public class FileMetaData {
    private String[] record;
    private final String[] headers;
    private final String[] ratesData;
    private final String pattern;

    public FileMetaData(String[] record, String[] headers, String pattern) {
        this.record = record;
        this.ratesData = Arrays.copyOfRange(this.record, 16, this.record.length);
        this.headers = headers;
        this.pattern = pattern;
    }

    public void setRecord(String[] record) {
        this.record = record;
    }

    public String[] getRecords() {
        return record;
    }

    public String[] getHeaders() {
        return headers;
    }

    public String getPattern(){
        return pattern;
    }

    public String[] getRatesData() {
        return ratesData;
    }
}
