package org.progressoft.impl;

import org.progressoft.interfaces.BisFileReader;
import org.progressoft.interfaces.BisPathValidator;
import org.progressoft.interfaces.Record;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Currency;
import java.util.List;

public class DefaultBisFileReader implements BisFileReader<CurrencyRecord> {

    private static final String ANNUAL_PATTERN = "\\d{4}";
    private static final String MONTHLY_PATTERN = "\\d{4}-\\d{1,2}";
    private static final String QUARTERLY_PATTERN = "\\d{4}-Q\\d{1}";
    private final BisPathValidator bisPathValidator;

    public DefaultBisFileReader() {
        this(new DefaultBisPathValidator());
    }

    public DefaultBisFileReader(BisPathValidator bisPathValidator) {
        this.bisPathValidator = bisPathValidator;
    }

    @Override
    public CurrencyRecord read(Path path) throws IOException {
        bisPathValidator.validatePath(path);
        List<String> allLines = Files.readAllLines(path);
        String[] headers = allLines.get(0).split(",");
        String[] dateHeaders = extractHeaders(headers);
        boolean first = true;
        CurrencyRecord rootRecord = null;
        for (int index = 1; index < allLines.size(); index++) {
            String[] record = allLines.get(index).split(",");
            String frequency = record[0];
            FileMetaData fileMetaData = new FileMetaData(record, dateHeaders, getPattern(frequency));
            if(first){
                rootRecord = new CurrencyRecord(fileMetaData);
                first = false;
                continue;
            }
            rootRecord.passFileData(fileMetaData);
        }
        return rootRecord;
    }

    private String getPattern(String frequency) {
        switch (frequency){
            case "A":
                return ANNUAL_PATTERN;
            case "M":
                return MONTHLY_PATTERN;
        }
        return QUARTERLY_PATTERN;
    }

    private String[] extractHeaders(String[] headers) {
        return Arrays.copyOfRange(headers, 16, headers.length);
    }
}
