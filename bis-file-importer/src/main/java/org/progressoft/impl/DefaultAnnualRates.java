package org.progressoft.impl;

import org.progressoft.interfaces.AnnualRates;
import org.progressoft.interfaces.BaseRates;
import java.time.Year;

public class DefaultAnnualRates extends BaseRates<Year> implements AnnualRates {
    public DefaultAnnualRates(FileMetaData fileMetaData) {
        super(fileMetaData);
    }

    @Override
    protected Year getKey(String header) {
        int year = Integer.parseInt(header);
        return Year.of(year);
    }
}
