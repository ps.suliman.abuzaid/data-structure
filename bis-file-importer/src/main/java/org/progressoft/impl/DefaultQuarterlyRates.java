package org.progressoft.impl;

import org.progressoft.YearQuarter;
import org.progressoft.interfaces.BaseRates;
import org.progressoft.interfaces.QuarterlyRates;

public class DefaultQuarterlyRates extends BaseRates<YearQuarter> implements QuarterlyRates {
    public DefaultQuarterlyRates(FileMetaData fileMetaData) {
        super(fileMetaData);
    }

    @Override
    protected YearQuarter getKey(String header) {
        String[] headerValues = header.split("-");
        int year = Integer.parseInt(headerValues[0]);
        int quarter = Integer.parseInt(headerValues[1].charAt(1) + "");
        return YearQuarter.of(year, quarter);
    }
}
