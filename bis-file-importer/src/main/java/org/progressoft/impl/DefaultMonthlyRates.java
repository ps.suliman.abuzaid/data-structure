package org.progressoft.impl;

import org.progressoft.interfaces.BaseRates;
import org.progressoft.interfaces.MonthlyRates;

import java.time.YearMonth;

public class DefaultMonthlyRates extends BaseRates<YearMonth> implements MonthlyRates {
    public DefaultMonthlyRates(FileMetaData fileMetaData) {
        super(fileMetaData);
    }

    @Override
    protected YearMonth getKey(String header) {
        String[] headerValues = header.split("-");
        int year = Integer.parseInt(headerValues[0]);
        int month = Integer.parseInt(headerValues[1]);
        return YearMonth.of(year, month);
    }
}
