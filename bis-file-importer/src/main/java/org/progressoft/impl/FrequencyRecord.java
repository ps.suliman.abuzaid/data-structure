package org.progressoft.impl;

import org.progressoft.Frequency;
import org.progressoft.interfaces.BaseRecord;
import org.progressoft.interfaces.Rates;


public class FrequencyRecord extends BaseRecord<Frequency, Rates<?>> {
    public FrequencyRecord(FileMetaData fileMetaData) {
        addFrequencyRates(fileMetaData);
    }

    private Rates<?> getRates(Frequency frequency, FileMetaData fileMetaData){
        switch (frequency) {
            case ANNUAL:
                return new DefaultAnnualRates(fileMetaData);
            case MONTHLY:
                return new DefaultMonthlyRates(fileMetaData);
        }
        return new DefaultQuarterlyRates(fileMetaData);
    }

    @Override
    public Rates<?> getRecord(Frequency key) {
        return super.getRecord(key);
    }

    @Override
    public void passFileData(FileMetaData fileMetaData) {
        addFrequencyRates(fileMetaData);
    }

    private void addFrequencyRates(FileMetaData fileMetaData) {
        String[] record = fileMetaData.getRecords();
        Frequency frequency = Frequency.byCode(record[0]);
        addRecord(frequency, getRates(frequency, fileMetaData));
    }
}