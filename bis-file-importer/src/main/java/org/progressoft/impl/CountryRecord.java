package org.progressoft.impl;

import org.progressoft.interfaces.BaseRecord;

import java.util.Objects;

public class CountryRecord extends BaseRecord<String, CollectionRecord> {
    public CountryRecord(FileMetaData fileMetaData) {
        String[] record = fileMetaData.getRecords();
        String countryCode = record[2];
        addRecord(countryCode, new CollectionRecord(fileMetaData));
    }

    @Override
    public void passFileData(FileMetaData fileMetaData) {
        String countryCode = fileMetaData.getRecords()[2];
        CollectionRecord collectionRecord = getRecord(countryCode);
        if(Objects.isNull(collectionRecord)){
            addRecord(countryCode, new CollectionRecord(fileMetaData));
            return;
        }
        collectionRecord.passFileData(fileMetaData);
    }
}
