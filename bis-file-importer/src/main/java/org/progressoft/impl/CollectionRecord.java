package org.progressoft.impl;

import org.progressoft.CollectionType;
import org.progressoft.interfaces.BaseRecord;

import java.util.Objects;

public class CollectionRecord extends BaseRecord<CollectionType, FrequencyRecord> {
    public CollectionRecord(FileMetaData fileMetaData) {
        String[] record = fileMetaData.getRecords();
        CollectionType collectionType = CollectionType.byCode(record[6]);
        addRecord(collectionType, new FrequencyRecord(fileMetaData));
    }

    @Override
    public void passFileData(FileMetaData fileMetaData) {
        String collectionTypeCode = fileMetaData.getRecords()[6];
        CollectionType collectionType = CollectionType.byCode(collectionTypeCode);
        FrequencyRecord frequencyRecord = getRecord(collectionType);
        if(Objects.isNull(frequencyRecord)){
            addRecord(collectionType, new FrequencyRecord(fileMetaData));
            return;
        }
        frequencyRecord.passFileData(fileMetaData);
    }
}
