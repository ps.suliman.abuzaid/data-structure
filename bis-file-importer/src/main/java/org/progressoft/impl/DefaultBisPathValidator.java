package org.progressoft.impl;

import org.progressoft.interfaces.BisPathValidator;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;

public class DefaultBisPathValidator implements BisPathValidator {
    public void validatePath(Path bisFilePath) {
        if (Objects.isNull(bisFilePath)) {
            throw new IllegalArgumentException("Bis file path must not be null");
        }
        if (Files.notExists(bisFilePath)) {
            throw new IllegalArgumentException("directory does not exist");
        }
        if(Files.isDirectory(bisFilePath)){
            throw new IllegalArgumentException("No file was found at the specified path");
        }
    }

}
