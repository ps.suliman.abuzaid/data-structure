package org.progressoft.exception;

public class IncorrectBisFileFormatException extends RuntimeException{
    public IncorrectBisFileFormatException() {
        super();
    }

    public IncorrectBisFileFormatException(String message) {
        super(message);
    }

    public IncorrectBisFileFormatException(String message, Throwable cause) {
        super(message, cause);
    }
}
