package org.progressoft;

import java.util.NoSuchElementException;

public enum Frequency {
    ANNUAL, MONTHLY, QUARTERLY;

    public static Frequency byCode(String frequencyCode){
        switch (frequencyCode) {
            case "A":
                return ANNUAL;
            case "M":
                return MONTHLY;
            case "Q":
                return QUARTERLY;
        }
        throw new NoSuchElementException("No frequency found for this code");
    }
}
