package org.progressoft;

import java.time.temporal.Temporal;
import java.time.temporal.TemporalField;
import java.time.temporal.TemporalUnit;
import java.util.Objects;

public class YearQuarter {
    private final int year;
    private final int quarter;

    private YearQuarter(int year, int quarter) {
        this.year = year;
        this.quarter = quarter;
    }

    public static YearQuarter of(int year, int quarter) {
        if(quarter < 1 || quarter > 4){
            throw new IllegalArgumentException("Invalid Quarter");
        }

        if(year < 0){
            throw new IllegalArgumentException("Invalid year");
        }

        return new YearQuarter(year, quarter);
    }

    public int getYear() {
        return year;
    }

    public int getQuarter() {
        return quarter;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        YearQuarter yearQuarter = (YearQuarter) o;
        return year == yearQuarter.year &&
                this.quarter == yearQuarter.quarter;
    }

    @Override
    public int hashCode() {
        return Objects.hash(year, quarter);
    }

    @Override
    public String toString() {
        return year + "-Q" + quarter;
    }
}
