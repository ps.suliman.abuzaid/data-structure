package org.progressoft.interfaces;

import org.progressoft.impl.FileMetaData;

public interface Record<P,R>{
    R getRecord(P key);
    void passFileData(FileMetaData fileMetaData);
}
