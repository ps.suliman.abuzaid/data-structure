package org.progressoft.interfaces;

import org.progressoft.impl.FileMetaData;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public abstract class BaseRates<K> implements Rates<K> {
    Map<K, BigDecimal> dateRateMap = new HashMap<>();

    public BaseRates(FileMetaData fileMetaData) {
        String[] ratesData = fileMetaData.getRatesData();
        String[] dateHeaders = fileMetaData.getHeaders();
        for (int index = 0; index < ratesData.length; index++) {
            String currentHeader = dateHeaders[index];
            if(ratesData[index].isEmpty() ||
                    ratesData[index].equalsIgnoreCase("NaN") ||
                    !currentHeader.matches(fileMetaData.getPattern())){
                continue;
            }
            K dateHeader = getKey(currentHeader);
            BigDecimal rate = new BigDecimal(ratesData[index]);
            dateRateMap.put(dateHeader, rate);
        }
    }

    protected abstract K getKey(String header);

    @Override
    public BigDecimal getRate(K dateKey) {
        return dateRateMap.get(dateKey);
    }
}
