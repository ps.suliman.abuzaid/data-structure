package org.progressoft.interfaces;

import org.progressoft.interfaces.Rates;
import org.progressoft.interfaces.Record;

import java.io.IOException;
import java.nio.file.Path;

public interface BisFileReader<R extends Record<?,?>> {

    R read(Path path) throws IOException;
}
