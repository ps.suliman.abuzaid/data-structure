package org.progressoft.interfaces;

import org.progressoft.impl.FileMetaData;

import java.util.HashMap;
import java.util.Map;

public abstract class BaseRecord<P,R> implements Record<P,R>{
    private final Map<P, R> recordMap = new HashMap<>();

    @Override
    public R getRecord(P key) {
        return recordMap.get(key);
    }

    public void addRecord(P key, R value){
        recordMap.put(key, value);
    }
    public void passFileData(FileMetaData fileMetaData){};
}
