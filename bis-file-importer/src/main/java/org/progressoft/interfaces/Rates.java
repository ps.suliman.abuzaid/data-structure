package org.progressoft.interfaces;

import java.math.BigDecimal;
import java.time.temporal.Temporal;

public interface Rates<K> {
    BigDecimal getRate(K dateKey);
}
