package org.progressoft.interfaces;

import org.progressoft.YearQuarter;

public interface QuarterlyRates extends Rates<YearQuarter>{
}
