package org.progressoft.interfaces;

import java.nio.file.Path;

public interface BisPathValidator {
    void validatePath(Path bisFilePath);
}
