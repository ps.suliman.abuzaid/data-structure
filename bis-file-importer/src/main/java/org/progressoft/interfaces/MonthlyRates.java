package org.progressoft.interfaces;

import java.time.YearMonth;

public interface MonthlyRates extends Rates<YearMonth>{
}
