package org.progressoft;

import org.progressoft.exception.IncorrectBisFileFormatException;
import org.progressoft.exception.RateNotFoundException;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

public class BisFileDataFetcher {

    private final Path bisFilePath;
    private final Map<String, BigDecimal> result;
    public BisFileDataFetcher(Path bisFilePath) throws IOException {
        validatePath(bisFilePath);
        this.bisFilePath = bisFilePath;
        result = new HashMap<>();
        getBisUsdToCurrencyExchangeRates();
    }

    private void validatePath(Path bisFilePath) {
        if (Objects.isNull(bisFilePath)) {
            throw new IllegalArgumentException("Bis file path must not be null");
        }
        if (Files.notExists(bisFilePath)) {
            throw new IllegalArgumentException("directory does not exist");
        }
        if(Files.isDirectory(bisFilePath)){
            throw new IllegalArgumentException("No file was found at the specified path");
        }
    }

    public void getBisUsdToCurrencyExchangeRates() throws IOException {
        List<String> allLines = Files.readAllLines(bisFilePath);
        for (int index = 1; index < allLines.size(); index++) {
            String line = allLines.get(index);
            String[] lineData = line.split(",");
            String currencyCode = lineData[2].substring(1,4);
            BigDecimal rate;
            try{
                rate = new BigDecimal(lineData[lineData.length - 1]);
            }catch (NumberFormatException formatException){
                throw new IncorrectBisFileFormatException("File has a text where a number is expected");
            }
            result.put(currencyCode, rate);
        }
    }

    public BigDecimal getCurrencyRateFromBase(Currency currency) {
        BigDecimal rate = result.get(currency.getCurrencyCode());
        if(Objects.isNull(rate)){
            throw new RateNotFoundException("No exchange rate found for this currency");
        }
        return rate;
    }
}
