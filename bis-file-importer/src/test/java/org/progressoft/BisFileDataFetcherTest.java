package org.progressoft;

import org.junit.jupiter.api.Test;
import org.progressoft.exception.IncorrectBisFileFormatException;
import org.progressoft.exception.RateNotFoundException;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Currency;

import static org.junit.jupiter.api.Assertions.*;

class BisFileDataFetcherTest {

    @Test
    void givenNullPath_whenConstructingDefaultBisFileDataFetcher_thenThrowIllegalArgumentException() {
        IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class,
                () -> new BisFileDataFetcher(null));
        assertEquals("Bis file path must not be null", illegalArgumentException.getMessage());
    }

    @Test
    void givenInvalidPathToFileThatDoesNotExist_whenConstructingDefaultBisFileDataFetcher_thenThrowIllegalArgumentException() {
        IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class,
                () -> new BisFileDataFetcher(Path.of("./hello/world")));
        assertEquals("directory does not exist", illegalArgumentException.getMessage());
    }

    @Test
    void givenValidPathWithNoFile_whenConstructingBisFileDataFetcher_thenThrowIllegalArgumentException() throws IOException {
        Path path = Files.createTempDirectory("bis2");
        Files.createDirectories(path);
        IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class, ()->new BisFileDataFetcher(path));
        assertEquals("No file was found at the specified path", illegalArgumentException.getMessage());
    }

    @Test
    void givenValidPathToFileWhereExchangeRateOfACurrencyIsNotNumber_whenConstructingBisFileDataFetcher_thenThrowIllegalArgumentException() throws IOException {
        Path path = copyFileToTempFile("bis", "/exchangeRatesTest.csv");
        IncorrectBisFileFormatException incorrectBisFileFormatException = assertThrows(IncorrectBisFileFormatException.class,
                () -> new BisFileDataFetcher(path).getBisUsdToCurrencyExchangeRates());
        assertEquals("File has a text where a number is expected", incorrectBisFileFormatException.getMessage());
    }

    @Test
    void givenValidPath_whenConstructingBisFileDataFetcher_thenReturnResult() throws IOException {
        Path path = copyFileToTempFile("testBis", "/exchangeRates.csv");
        BisFileDataFetcher bisFileDataFetcher = new BisFileDataFetcher(path);
        assertNotNull(bisFileDataFetcher, "result must not be null");

        Currency jod = Currency.getInstance("JOD");
        Currency syr = Currency.getInstance("SAR");
        Currency jpy = Currency.getInstance("JPY");

        assertEquals(new BigDecimal("0.71"), bisFileDataFetcher
                .getCurrencyRateFromBase(jod));
        assertEquals(new BigDecimal("3.75"), bisFileDataFetcher.getCurrencyRateFromBase(syr));
        assertEquals(new BigDecimal("108.545487"), bisFileDataFetcher.getCurrencyRateFromBase(jpy));
    }

    @Test
    void givenValidCurrencyWithNoExchangeRate_whenGetCurrencyRateFromBase_thenThrowRateNotFoundException() throws IOException {
        Path path = copyFileToTempFile("bis", "/exchangeRatesWithMissingRates.csv");
        BisFileDataFetcher bisFileDataFetcher = new BisFileDataFetcher(path);
        Currency jod=Currency.getInstance("JOD");
        RateNotFoundException rateNotFoundException = assertThrows(RateNotFoundException.class, ()-> bisFileDataFetcher.getCurrencyRateFromBase(jod));
        assertEquals("No exchange rate found for this currency", rateNotFoundException.getMessage());
    }

    private Path copyFileToTempFile(String tempFileName, String resourceFileName) throws IOException {
        Path path = Files.createTempFile(tempFileName, ".csv");
        try (InputStream resourceAsStream = this.getClass().getResourceAsStream(resourceFileName);
             OutputStream outputStream = Files.newOutputStream(path)) {
            int read;
            while (resourceAsStream != null && (read = resourceAsStream.read()) != -1) {
                outputStream.write(read);
            }
        }
        return path;
    }

}