package org.progressoft.impl;

import org.junit.jupiter.api.Test;
import org.progressoft.CollectionType;
import org.progressoft.Frequency;
import org.progressoft.interfaces.BisFileReader;
import org.progressoft.interfaces.Rates;
import org.progressoft.interfaces.Record;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Year;
import java.util.Currency;

import static org.junit.jupiter.api.Assertions.*;

class DefaultBisFileReaderTest {
    private final BisFileReader<CurrencyRecord> bisFileReader = new DefaultBisFileReader();

    @Test
    void givenValidPath_whenRead_thenReturnResult() throws IOException {
        Path path = copyFileToTempFile("bis", "/newExchangeRates.csv");
        Record<Currency,CountryRecord> result = bisFileReader.read(path);
        assertNotNull(result);
        CountryRecord jodCountryRecord = result.getRecord(Currency.getInstance("JOD"));
        assertNotNull(jodCountryRecord);
        CollectionRecord jodCollectionRecord = jodCountryRecord.getRecord("JO");
        assertNotNull(jodCollectionRecord);
        FrequencyRecord jodFrequencyRecord = jodCollectionRecord.getRecord(CollectionType.EOP);
        assertNotNull(jodFrequencyRecord);
        Rates<Year> rates = (Rates<Year>) jodFrequencyRecord.getRecord(Frequency.ANNUAL);
        assertNotNull(rates);
        assertEquals(rates.getRate(Year.of(2022)), new BigDecimal("0.71"));
    }

    private Path copyFileToTempFile(String tempFileName, String resourceFileName) throws IOException {
        Path path = Files.createTempFile(tempFileName, ".csv");
        try (InputStream resourceAsStream = this.getClass().getResourceAsStream(resourceFileName);
             OutputStream outputStream = Files.newOutputStream(path)) {
            int read;
            while (resourceAsStream != null && (read = resourceAsStream.read()) != -1) {
                outputStream.write(read);
            }
        }
        return path;
    }

}