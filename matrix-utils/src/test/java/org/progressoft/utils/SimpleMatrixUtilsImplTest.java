package org.progressoft.utils;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.progressoft.classes.Matrix;

import static org.junit.jupiter.api.Assertions.*;

class SimpleMatrixUtilsImplTest {

    private MatrixUtilities matrixUtil;
    @BeforeEach
    public void setUp() {
        matrixUtil = new SimpleMatrixUtilsImpl();
    }

    @Test
    void sumMatrix() {
        Matrix matrixOne = new Matrix(new int[][]{
                {3,4},
                {7,2}
        });

        Matrix matrixTwo = new Matrix(new int[][] {
                {1,2},
                {5,6},
        });


        Matrix expectedResult = new Matrix(new int[][]{
                {4,6},
                {12,8},
        });


        Matrix actualResult = matrixUtil.sumMatrices(matrixTwo, matrixOne);
        assertEquals(expectedResult,actualResult);
    }

    @Test
    void givenInvalidMatrixInputtedLength_whenSubMatrix_thenThrowException() {
        Matrix matrixOne = new Matrix(new int[][]{
                {3,4},
                {7,2}
        });

        Matrix matrixTwo = new Matrix(new int[][]{
                {1,2}
        });

        IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class, () -> matrixUtil.sumMatrices(matrixTwo, matrixOne));
        assertEquals("Matrix arguments should have the same number of rows and columns", illegalArgumentException.getMessage());
    }

    @Test
    void givenInvalidNullMatrixOne_whenSumMatrix_thenThrowNullPointerException(){
        Matrix matrixTwo= new Matrix(new int[][]{
                {1,2},
                {3,4}
        });

        NullPointerException nullPointerException = assertThrows(NullPointerException.class, () -> matrixUtil.sumMatrices(null, matrixTwo));
        assertEquals("Matrix input can not be null", nullPointerException.getMessage());
    }

    @Test
    void givenMatrixOneAndEmptyMatrixTwoInputs_whenSumMatrix_thenThrowIllegalArgumentException(){
        Matrix matrixOne = new Matrix(new int[][]{
                {1,2},
                {3,4}
        });

        IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class, () -> matrixUtil.sumMatrices(matrixOne, new Matrix(new int[][]{})));
        assertEquals("Matrix is empty", illegalArgumentException.getMessage());
    }

    @Test
    void givenInvalidMatrixOneWithNullRow_whenSumMatrix_thenThrowIllegalArgumentException(){
        Matrix matrixTwo= new Matrix(new int[][]{
                {2,3,4},
                {5,6,7}
        });

        IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class, () -> matrixUtil.sumMatrices(new Matrix(new int[][]{
                {1,2},
                null,
                {3,4}
        }), matrixTwo));
        assertEquals("Matrix has null rows", illegalArgumentException.getMessage());
    }

    @Test
    void givenInvalidMatrixTwoWithUnequalColumns_whenSumMatrix_thenThrowIllegalArgumentException(){
        Matrix matrixOne = new Matrix(new int[][]{
                {1,2},
                {3,4}
        });

        IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class, () -> matrixUtil.sumMatrices(matrixOne, new Matrix(new int[][]{
                {2,3,4},
                {5,6,7},
                {2,3}
        })));
        assertEquals("Matrix has rows with unequal column lengths", illegalArgumentException.getMessage());
    }

    @Test
    void multiplyMatrixByNum() {
        Matrix matrixOne = new Matrix(new int[][]{
                {3,4},
                {7,2}
        });

        int multiplier = 2;


        Matrix expectedResult = new Matrix(new int[][]{
                {6,8},
                {14,4}
        });

        Matrix actualResult = matrixUtil.multiplyMatrixByNum(matrixOne, multiplier);

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void flipMatrix() {
        Matrix matrix= new Matrix(new int[][]{
                {1,2,3,4},
                {5,6,7,8}
        });


        Matrix expectedResult = new Matrix(new int[][]{
                {1,5},
                {2,6},
                {3,7},
                {4,8}
        });
        Matrix actualResult = matrixUtil.flipMatrix(matrix);

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void multiplyMatrices() {

        Matrix matrixOne = new Matrix(new int[][]{
                {3,4},
                {7,2}
        });

        Matrix matrixTwo= new Matrix(new int[][]{
                {1,2},
                {5,6},
        });


        Matrix expectedResult = new Matrix(new int[][]{
                {23,30},
                {17,26},
        });
        Matrix actualResult = matrixUtil.multiplyMatrices(matrixOne, matrixTwo);

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void givenMatrixOneWithColumnsUnequalToMatrixTwoRows_WhenMultiplyMatrices_ThenThrowException(){
        Matrix matrixOne = new Matrix(new int[][]{
                {3,4},
                {7,2}
        });

        Matrix matrixTwo= new Matrix(new int[][]{
                {1,2,3},
                {5,6,7},
                {8,9,4}
        });

        IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class, ()-> matrixUtil.multiplyMatrices(matrixOne, matrixTwo));
        assertEquals("Matrix 1 columns must equal Matrix 2 Rows",illegalArgumentException.getMessage());
    }

    @Test
    void givenTwoValidMatrices_whenSubMatrix_thenReturnResult() {
        Matrix matrix = new Matrix(new int[][]{
                {1,2,3,4,5},
                {5,6,7,6,7},
                {8,9,4,2,3}
        });

        Matrix expectedResult = new Matrix(new int[][]{
                {1,2,3,4},
                {5,6,7,6}
        });

        Matrix actualResult = matrixUtil.subMatrix(matrix,3, 5);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void givenMatrixWithInvalidRowToRemoveAndColumnToRemoveArguments_whenSubMatrix_thenActualResultShouldEqualExpected(){
        Matrix matrix = new Matrix(new int[][]{
                {1,2,3,4,5},
                {5,6,7,6,7},
                {8,9,4,2,3}
        });

        Matrix expectedResult = new Matrix(new int[][]{
                {1,2,3,4,5},
                {5,6,7,6,7},
                {8,9,4,2,3}
        });

        Matrix actualResult = matrixUtil.subMatrix(matrix,10, -1);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void toDiagonalMatrix() {
        Matrix matrix = new Matrix(new int[][]{
                {1,2,3},
                {4,5,6},
                {7,8,9}
        });

        Matrix expectedResult = new Matrix(new int[][]{
                {1,0,0},
                {0,5,0},
                {0,0,9}
        });

        Matrix actualResult = matrixUtil.toDiagonalMatrix(matrix);

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void givenInvalidMatrixInputted_WhenToDiagonalMatrix_ThenThrowException(){
        Matrix matrix = new Matrix(new int[][]{
                {1,2,3},
                {4,5,6}
        });

        IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class, ()-> matrixUtil.toDiagonalMatrix(matrix));
        assertEquals("Matrix must be a square matrix", illegalArgumentException.getMessage());
    }

    @Test
    void toLowerTriangularMatrix() {
        Matrix matrix = new Matrix(new int[][]{
                {1,2,3},
                {4,5,6},
                {7,8,9}
        });

        Matrix expectedResult= new Matrix(new int[][]{
                {1,0,0},
                {4,5,0},
                {7,8,9}
        });

        Matrix actualResult = matrixUtil.toLowerTriangularMatrix(matrix);

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void givenInvalidMatrixInput_WhenToLowerTriangleMatrix_ThenThrowException(){
        Matrix matrix = new Matrix(new int[][]{
                {1,2,3},
                {4,5,6}
        });

        IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class, ()-> matrixUtil.toLowerTriangularMatrix(matrix));
        assertEquals("Matrix must be a square matrix", illegalArgumentException.getMessage());
    }

    @Test
    void toUpperTriangularMatrix() {
        Matrix matrix = new Matrix(new int[][]{
                {1,2,3},
                {4,5,6},
                {7,8,9}
        });

        Matrix expectedResult= new Matrix(new int[][]{
                {1,2,3},
                {0,5,6},
                {0,0,9}
        });

        Matrix actualResult = matrixUtil.toUpperTriangularMatrix(matrix);

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void givenInvalidMatrixInput_WhenToUpperTriangleMatrix_ThenThrowException(){
        Matrix matrix = new Matrix(new int[][]{
                {1,2,3},
                {4,5,6}
        });

        IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class, ()-> matrixUtil.toUpperTriangularMatrix(matrix));
        assertEquals("Matrix must be a square matrix", illegalArgumentException.getMessage());
    }

    @Test
    void findMatrixDeterminant() {
        Matrix matrix = new Matrix(new int[][]{
                {3,4,3,2},
                {5,2,1,5},
                {6,4,7,4},
                {8,5,6,8}
        });

        int expected = -97;

        int actualResult = matrixUtil.findMatrixDeterminant(matrix);
        assertEquals(expected, actualResult);

    }

    @Test
    void givenInvalidNonSquareMatrix_WhenFindMatrixDeterminant_ThenThrowException() {
        Matrix matrix = new Matrix(new int[][]{
                {3,4,3,2},
                {5,2,1,5},
                {6,4,7,4}
        });

        IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class, ()-> matrixUtil.findMatrixDeterminant(matrix));
        assertEquals("Matrix must be a square matrix", illegalArgumentException.getMessage());

    }

    @Test
    void givenTwoValidMatrixObjects_whenSumMatrix_thenReturnSummedMatrix(){
        Matrix matrix1 = new Matrix(new int[][] {
                {1,2,3},
                {4,5,6}
        });

        Matrix matrix2 = new Matrix(new int[][]{
                {3,2,1},
                {6,5,4}
        });

        Matrix expected = new Matrix(new int[][] {
                {4,4,4},
                {10,10,10}
        });

        Matrix actualResult = matrixUtil.sumMatrices(matrix1, matrix2);
        assertEquals(expected, actualResult);
    }


    @Test
    void givenMatrixOneWithColumnsNotEqualToMatrixTwoColumns_whenSumMatrices_thenThrowException() {
        Matrix matrix1WithThreeRowsAndFourColumn = new Matrix(new int[][]{
                {1, 2, 3, 4},
                {6, 7, 8, 9},
                {6, 7, 8, 9}
        });

        Matrix matrix2WithThreeRowsAndThreeColumn = new Matrix(new int[][]{
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        });
        IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class, () -> matrixUtil.sumMatrices(matrix1WithThreeRowsAndFourColumn, matrix2WithThreeRowsAndThreeColumn));
        assertEquals("Matrix arguments should have the same number of rows and columns", illegalArgumentException.getMessage());

        Matrix matrix1WithThreeRowsAndThreeColumn = new Matrix(new int[][]{
                {1, 2, 3},
                {6, 7, 8},
                {6, 7, 8},
        });

        Matrix matrix2WithTwoRowsAndThreeColumn = new Matrix(new int[][]{
                {1, 2, 3},
                {4, 5, 6}
        });
        illegalArgumentException = assertThrows(IllegalArgumentException.class, () -> matrixUtil.sumMatrices(matrix1WithThreeRowsAndThreeColumn, matrix2WithTwoRowsAndThreeColumn));
        assertEquals("Matrix arguments should have the same number of rows and columns", illegalArgumentException.getMessage());
    }

    @Test
    void givenMatrixOneObjectWithRowsAndColumnsNotEqualToMatrixTwoObjectRowsAndColumns_whenSumMatrices_thenThrowException() {
        Matrix matrix1 = new Matrix(new int[][]{
                {1, 2, 3, 4},
                {6, 7, 8, 9},
        });

        Matrix matrix2 = new Matrix(new int[][]{
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        });
        IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class, () -> matrixUtil.sumMatrices(matrix1, matrix2));
        assertEquals("Matrix arguments should have the same number of rows and columns", illegalArgumentException.getMessage());
    }

    @Test
    void givenMatrixOneWithColumnUnequalToMatrixTwoRow_whenMultiplyMatrices_thenThrowNewException() {
        Matrix matrix1 = new Matrix(new int[][]{
                {1, 2},
                {3, 4}
        });

        Matrix matrix2 = new Matrix(new int[][]{
                {1, 2}
        });

        IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class, () -> matrixUtil.multiplyMatrices(matrix1, matrix2));
        assertEquals("Matrix 1 columns must equal Matrix 2 Rows", illegalArgumentException.getMessage());

    }
}