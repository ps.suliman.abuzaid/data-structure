package org.progressoft.utils;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SimpleMatrixValidatorImplTest {
    private MatrixValidator matrixValidator;
    @BeforeEach
    public void setUp(){
        matrixValidator = new SimpleMatrixValidatorImpl();
    }

    @Test
    void givenNullMatrix_whenIsMatrixNull_thenThrowException() {
        NullPointerException nullPointerException = assertThrows(NullPointerException.class, () -> matrixValidator.validateMatrix(null));
        assertEquals("Matrix is null", nullPointerException.getMessage());
    }

    @Test
    void givenEmptyMatrix_whenIsMatrixEmpty_thenThrowException() {
        int[][] matrix = {};
        IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class, () -> matrixValidator.isMatrixEmpty(matrix));
        assertEquals("Matrix is empty", illegalArgumentException.getMessage());
    }

    @Test
    void givenMatrixWithNullOrEmptyRows_whenValidateMatrixRows_thenThrowException() {
        int[][] matrixWithNullRow = {
                {1, 2},
                null
        };
        int[][] matrixWithEmptyRow = {
                {1, 2},
                {}
        };
        IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class, () -> matrixValidator.validateMatrixRows(matrixWithNullRow));
        assertEquals("Matrix has null rows", illegalArgumentException.getMessage());

        illegalArgumentException = assertThrows(IllegalArgumentException.class, () -> matrixValidator.validateMatrixRows(matrixWithEmptyRow));
        assertEquals("Matrix has empty rows", illegalArgumentException.getMessage());
    }

    @Test
    void givenMatrixWithRowsThatHaveUnequalColumnLength_whenValidateMatrixRows_thenThrowException() {
        int[][] matrixWithUnequalColumnLength = {
                {1, 2, 3, 4},
                {5},
                {6, 7, 8},
                {9, 10}
        };
        IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class, () -> matrixValidator.validateMatrixRows(matrixWithUnequalColumnLength));
        assertEquals("Matrix has rows with unequal column lengths", illegalArgumentException.getMessage());
    }

}