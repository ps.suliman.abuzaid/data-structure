package org.progressoft.classes;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MatrixTest {

    @Test
    void givenValidMatrixInput_whenNewMatrix_thenCreateMatrixObject(){
        Matrix matrix = new Matrix(new int[][]{
                {1,2,3},
                {4,5,6},
                {7,8,9}
        });
        assertEquals(3, matrix.getValue(0,2));
        assertEquals(3, matrix.getRows());
        assertEquals(3, matrix.getColumns());
    }

    @Test
    void givenInvalidNullMatrixInput_whenNewMatrixObject_thenThrowException(){
        NullPointerException nullPointerException = assertThrows(NullPointerException.class, ()-> new Matrix(null));
        assertEquals("Matrix is null", nullPointerException.getMessage());
    }
    @Test
    void givenInvalidEmptyMatrixInput_whenNewMatrixObject_thenThrowException(){
        IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class, ()-> new Matrix(new int[][]{}));
        assertEquals("Matrix is empty", illegalArgumentException.getMessage());
    }
    @Test
    void givenInvalidMatrixWithEmptyRowInput_whenNewMatrixObject_thenThrowException(){
        IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class, ()-> new Matrix(new int[][]{
                {1,2,3},
                {}
        }));
        assertEquals("Matrix has empty rows", illegalArgumentException.getMessage());
    }

    @Test
    void givenInvalidMatrixWithNullRowInput_whenNewMatrixObject_thenThrowException(){
        IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class, ()-> new Matrix(new int[][]{
                {1,2,3},
                null
        }));
        assertEquals("Matrix has null rows", illegalArgumentException.getMessage());
    }

    @Test
    void givenInvalidMatrixWhereRowsHaveUnequalColumnsInput_whenNewMatrixObject_thenThrowException(){
        IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class, ()-> new Matrix(new int[][]{
                {1,2,3},
                {2,3},
                {1}
        }));
        assertEquals("Matrix has rows with unequal column lengths", illegalArgumentException.getMessage());
    }
    @Test
    void givenTwoValidEqualMatrices_whenEquals_thenReturnTrue(){
        Matrix matrix1 = new Matrix(new int[][]{
                {1,2},
                {3,4}
        });

        Matrix matrix2 = new Matrix(new int[][]{
                {1,2},
                {3,4}
        });

        assertTrue(matrix1.equals(matrix2), "Matrices should be equal");
        assertEquals(matrix1.equals(matrix2), matrix2.equals(matrix1), "Matrices should be equal");
    }

    @Test
    void givenTwoValidEqualMatrices_whenHashCode_thenReturnTrue(){
        Matrix matrix1 = new Matrix(new int[][]{
                {1,2},
                {3,4}
        });

        Matrix matrix2 = new Matrix(new int[][]{
                {1,2},
                {3,4}
        });

        assertEquals(matrix1.hashCode(), matrix2.hashCode(), "Should have the same hashCode");
    }

    @Test
    void givenTwoValidUnequalMatrices_whenEquals_thenReturnFalse(){
        Matrix matrix1 = new Matrix(new int[][]{
                {1,2},
                {3,4}
        });

        Matrix matrix2 = new Matrix(new int[][]{
                {1,2,8},
                {3,4,8}
        });

        assertNotEquals(matrix1, matrix2);
    }

    @Test
    void givenOneValidMatrixAndObjectsFromDifferentTypes_whenEquals_thenReturnFalse(){
        Matrix matrix = new Matrix(new int[][]{
                {1,2},
                {3,4}
        });

        assertNotEquals(matrix,null);
        assertNotEquals("Hello", matrix);
        assertNotEquals(new Thread(), matrix);
    }
}