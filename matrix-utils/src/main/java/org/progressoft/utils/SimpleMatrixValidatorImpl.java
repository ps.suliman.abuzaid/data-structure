package org.progressoft.utils;

import org.progressoft.classes.Matrix;

import java.util.Objects;

public class SimpleMatrixValidatorImpl implements MatrixValidator {

    @Override
    public void isMatrixNull(int[][] matrix){
        if (!Objects.isNull(matrix)) {
            return;
        }
        throw new NullPointerException("Matrix is null");
    }

    @Override
    public void isMatrixEmpty(int[][] matrix){
        if (matrix.length > 0) {
            return;
        }
        throw new IllegalArgumentException("Matrix is empty");
    }

    @Override
    public void validateMatrixRows(int[][] matrix){
        int firstRowColLength = matrix[0].length;
        for (int[] row : matrix) {
            if (Objects.isNull(row)) {
                throw new IllegalArgumentException("Matrix has null rows");
            }

            if(row.length == 0){
                throw new IllegalArgumentException(("Matrix has empty rows"));
            }

            if (row.length != firstRowColLength) {
                throw new IllegalArgumentException("Matrix has rows with unequal column lengths");
            }
        }
    }

    @Override
    public void validateMatrix(int[][] matrix){
        isMatrixNull(matrix);
        isMatrixEmpty(matrix);
        validateMatrixRows(matrix);
    }

}
