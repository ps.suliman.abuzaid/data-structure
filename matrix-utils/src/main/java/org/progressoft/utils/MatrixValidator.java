package org.progressoft.utils;

import org.progressoft.classes.Matrix;

public interface MatrixValidator {

    void isMatrixNull(int[][] matrix);

    void isMatrixEmpty(int[][] matrix);

    void validateMatrixRows(int[][] matrix);
    void validateMatrix(int[][] matrix);
}
