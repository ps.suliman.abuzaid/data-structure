package org.progressoft.utils;

import org.progressoft.classes.Matrix;

import java.util.function.BiPredicate;

public class SimpleMatrixUtilsImpl implements MatrixUtilities {

    public Matrix sumMatrices(Matrix matrix1, Matrix matrix2) {
        validateIfMatricesAreNull(matrix1, matrix2);
        int matrix1Rows = matrix1.getRows();
        int matrix1Columns = matrix1.getColumns();

        validateIfRowsAndColumnsAreEqual(matrix1, matrix2);

        Matrix result = new Matrix(matrix1Rows, matrix1Columns);

        for (int row = 0; row < matrix1Rows; row++) {
            for (int column = 0; column < matrix1Columns; column++) {
                result.setValue(row, column, matrix1.getValue(row, column) + matrix2.getValue(row, column));
            }
        }
        return result;
    }

    @Override
    public Matrix multiplyMatrixByNum(Matrix matrix, int multiplier) {
        validateIfMatricesAreNull(matrix);
        int matrixRows = matrix.getRows(), matrixColumns = matrix.getColumns();

        Matrix result = new Matrix(matrixRows, matrixColumns);

        for (int row = 0; row < matrixRows; row++) {
            for (int column = 0; column < matrixColumns; column++) {
                result.setValue(row, column, matrix.getValue(row, column) * multiplier);
            }
        }
        return result;
    }

    @Override
    public Matrix flipMatrix(Matrix matrix) {
        validateIfMatricesAreNull(matrix);
        int matrixColumns = matrix.getColumns();
        int matrixRows = matrix.getRows();

        Matrix flippedMatrix = new Matrix(matrixColumns, matrixRows);

        for (int row = 0; row < matrixRows; row++) {
            for (int column = 0; column < matrixColumns; column++) {
                flippedMatrix.setValue(column, row, matrix.getValue(row, column));
            }
        }

        return flippedMatrix;
    }

    @Override
    public Matrix multiplyMatrices(Matrix matrix1, Matrix matrix2) {
        validateIfMatricesAreNull(matrix1, matrix2);
        int matrix1Rows = matrix1.getRows();
        int matrix2Rows = matrix2.getRows();
        int matrix1Columns = matrix1.getColumns();
        int matrix2Columns = matrix2.getColumns();

        validateIfMatricesCanBeMultiplied(matrix1Columns, matrix2Rows);

        Matrix result = new Matrix(matrix1Rows, matrix2Columns);
        int sum;
        for (int matrix1Row = 0; matrix1Row < matrix1Rows; matrix1Row++) {
            for (int matrix2Column = 0; matrix2Column < matrix2Columns; matrix2Column++) {
                sum = 0;
                for (int matrix1Column = 0; matrix1Column < matrix1Columns; matrix1Column++) {
                    sum += matrix1.getValue(matrix1Row, matrix1Column) * matrix2.getValue(matrix1Column, matrix2Column);
                }
                result.setValue(matrix1Row, matrix2Column, sum);
            }
        }

        return result;
    }

    @Override
    public Matrix subMatrix(Matrix matrix,int rowToRemove, int columnToRemove) {
        validateIfMatricesAreNull(matrix);
        int matrixRows = matrix.getRows();
        int matrixColumns = matrix.getColumns();
        int subMatrixRows = rowToRemove < 0 || rowToRemove > matrixRows ? matrixRows : matrixRows - 1;
        int subMatrixColumns = columnToRemove < 0 || columnToRemove > matrixColumns ? matrixColumns : matrixColumns - 1;
        Matrix result = new Matrix(subMatrixRows, subMatrixColumns);

        for (int row = 0, subMatrixRow = 0; row < matrixRows; row++) {
            if (row == rowToRemove - 1) continue;

            for (int column = 0, subMatrixColumn = 0; column < matrixColumns; column++) {
                if (column == columnToRemove - 1) continue;
                result.setValue(subMatrixRow, subMatrixColumn++, matrix.getValue(row, column));
            }
            subMatrixRow++;
        }

        return result;
    }

    private Matrix zeroMatrixElements(Matrix matrix, BiPredicate<Integer, Integer> testCondition) {
        validateIfMatricesAreNull(matrix);
        isSquareMatrix(matrix);
        int size = matrix.getRows();

        Matrix result = new Matrix(size, size);
        for (int row = 0; row < size; row++) {
            for (int column = 0; column < size; column++) {
                if (testCondition.test(column, row)) {
                    result.setValue(row, column, matrix.getValue(row, column));
                }
            }
        }

        return result;
    }

    @Override
    public Matrix toDiagonalMatrix(Matrix matrix) {
        return zeroMatrixElements(matrix, Integer::equals);
    }

    @Override
    public Matrix toLowerTriangularMatrix(Matrix matrix) {
        return zeroMatrixElements(matrix, (column, row) -> (column < row || column.equals(row)));
    }

    @Override
    public Matrix toUpperTriangularMatrix(Matrix matrix) {
        return zeroMatrixElements(matrix, (column, row) -> (column > row || column.equals(row)));
    }

    private void extractCofactor(Matrix matrix, int size, int row, Matrix temp) {
        for (int sRow = 1; sRow < size; sRow++) {
            for (int sCol = 0; sCol < size; sCol++) {
                if (sCol < row) {
                    temp.setValue(sRow - 1, sCol, matrix.getValue(sRow, sCol));
                } else if (sCol > row) {
                    temp.setValue(sRow - 1, sCol - 1, matrix.getValue(sRow, sCol));
                }
            }
        }
    }

    @Override
    public int findMatrixDeterminant(Matrix matrix) {
        validateIfMatricesAreNull(matrix);
        isSquareMatrix(matrix);
        int size = matrix.getRows();

        if (size == 1) {
            return matrix.getValue(0,0);
        }

        int determinant = 0, sign = 1;

        for (int row = 0; row < size; row++) {
            Matrix temp = new Matrix(size - 1, size - 1);
            extractCofactor(matrix, size, row, temp);
            determinant += sign * matrix.getValue(0,row) * findMatrixDeterminant(temp);
            sign = -sign;
        }
        return determinant;
    }

    private void isSquareMatrix(Matrix matrix) {
        int rowLength = matrix.getRows(), colLength = matrix.getColumns();
        if (rowLength == colLength) {
            return;
        }
        throw new IllegalArgumentException("Matrix must be a square matrix");
    }

    private void validateIfRowsAndColumnsAreEqual(Matrix matrix1, Matrix matrix2){
        int matrix1Rows = matrix1.getRows();
        int matrix2Rows = matrix2.getRows();
        int matrix1Columns = matrix1.getColumns();
        int matrix2Columns = matrix2.getColumns();

        if(matrix1Rows != matrix2Rows || matrix1Columns != matrix2Columns){
            throw new IllegalArgumentException("Matrix arguments should have the same number of rows and columns");
        }
    }

    private void validateIfMatricesCanBeMultiplied(int matrix1Columns, int matrix2Rows){
        if (matrix1Columns != matrix2Rows) {
            throw new IllegalArgumentException("Matrix 1 columns must equal Matrix 2 Rows");
        }
    }

    private void validateIfMatricesAreNull(Matrix ...matrices){
        for (Matrix matrix : matrices) {
            if(matrix == null){
                throw new NullPointerException("Matrix input can not be null");
            }
        }
    }
}
