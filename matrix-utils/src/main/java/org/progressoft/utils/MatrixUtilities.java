package org.progressoft.utils;

import org.progressoft.classes.Matrix;

public interface MatrixUtilities {
    Matrix sumMatrices(Matrix matrix1, Matrix matrix2);
    Matrix multiplyMatrixByNum(Matrix matrix, int multiplier);

    Matrix flipMatrix(Matrix matrix);

    Matrix multiplyMatrices(Matrix matrix1, Matrix matrix2);

    Matrix subMatrix(Matrix matrix, int columnToRemove, int rowToRemove);

    Matrix toDiagonalMatrix(Matrix matrix);
    Matrix toLowerTriangularMatrix(Matrix matrix);
    Matrix toUpperTriangularMatrix(Matrix matrix);

    int findMatrixDeterminant(Matrix matrix);

}
