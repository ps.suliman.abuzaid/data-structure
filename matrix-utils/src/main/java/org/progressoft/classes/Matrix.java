package org.progressoft.classes;

import org.progressoft.utils.MatrixValidator;
import org.progressoft.utils.SimpleMatrixValidatorImpl;

import java.util.Arrays;

public class Matrix {
    private final int[][] matrix;
    private final int rows;
    private final int columns;


    public Matrix(int[][] matrix) {
        this(matrix, new SimpleMatrixValidatorImpl());
    }

    public Matrix(int[][] matrix, MatrixValidator matrixValidator) {
        matrixValidator.validateMatrix(matrix);
        rows = matrix.length;
        columns = matrix[0].length;
        this.matrix = matrix;
    }

    public Matrix(int rows, int columns) {
        this.rows = rows;
        this.columns = columns;
        matrix = new int[rows][columns];
    }

    public int getRows() {
        return rows;
    }

    public int getColumns() {
        return columns;
    }

    public void setValue(int row, int column, int value){
        matrix[row][column] = value;
    }

    public int getValue(int row, int column) {
        return matrix[row][column];
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Matrix matrix1 = (Matrix) o;

        if (rows != matrix1.getRows()) return false;
        if (columns != matrix1.getColumns()) return false;
        return Arrays.deepEquals(matrix, matrix1.matrix);
    }

    @Override
    public int hashCode() {
        int result = Arrays.deepHashCode(matrix);
        result = 31 * result + rows;
        result = 31 * result + columns;
        return result;
    }
}
