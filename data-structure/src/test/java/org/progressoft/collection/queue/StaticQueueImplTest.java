package org.progressoft.collection.queue;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StaticQueueImplTest {

    @Test
    void givenValidInput_whenEnqueue_thenReturnEnqueuedElement() {
        Queue<Integer> staticQueue = new StaticQueueImpl<>();
        int expected = 10;
        int actual = staticQueue.enqueue(expected);
        assertEquals(expected, actual);
    }

    @Test
    void givenFullQueue_whenEnqueue_thenThrowException(){
        Queue<Integer> staticQueue = new StaticQueueImpl<>(3);
        staticQueue.enqueue(1);
        staticQueue.enqueue(2);
        staticQueue.enqueue(3);

        IllegalStateException illegalStateException = assertThrows(IllegalStateException.class, ()->staticQueue.enqueue(4));
        assertEquals("Queue is full", illegalStateException.getMessage());
    }

    @Test
    void givenValidQueue_whenPeek_thenReturnFirstElement() {
        Queue<Integer> staticQueue = new StaticQueueImpl<>(3);
        staticQueue.enqueue(1);
        staticQueue.enqueue(2);
        staticQueue.enqueue(3);

        assertEquals(1, staticQueue.peek());
    }

    @Test
    void givenEmptyQueue_whenPeek_thenReturnNull(){
        Queue<Integer> staticQueue = new StaticQueueImpl<>(3);
        assertNull(staticQueue.peek());
    }

    @Test
    void givenValidQueue_whenDequeue_thenRemoveHeadElement() {
        Queue<Integer> staticQueue = new StaticQueueImpl<>(3);
        staticQueue.enqueue(1);
        staticQueue.enqueue(2);
        staticQueue.enqueue(3);

        int removedElement = staticQueue.dequeue();
        assertEquals(1, removedElement);
    }

    @Test
    void givenQueueWhereFirstIdxIsOutOfBound_whenDequeue_thenRemoveHeadElement() {
        Queue<Integer> staticQueue = new StaticQueueImpl<>(2);
        staticQueue.enqueue(1);
        staticQueue.enqueue(2);
        staticQueue.dequeue();
        staticQueue.enqueue(3);
        staticQueue.dequeue();
        int removedElement = staticQueue.dequeue();
        assertEquals(3, removedElement);
    }

    @Test
    void givenEmptyQueue_whenDeque_thenReturnNull(){
        Queue<Integer> staticQueue = new StaticQueueImpl<>();
        assertNull(staticQueue.dequeue());
    }


    @Test
    void givenQueueWith3Elements_whenSize_thenReturn3() {
        Queue<Integer> staticQueue = new StaticQueueImpl<>(3);
        staticQueue.enqueue(1);
        staticQueue.enqueue(2);
        staticQueue.enqueue(3);

        assertEquals(3, staticQueue.size());
    }
}