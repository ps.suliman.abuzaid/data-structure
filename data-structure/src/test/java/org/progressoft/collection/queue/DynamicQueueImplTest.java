package org.progressoft.collection.queue;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DynamicQueueImplTest {

    @Test
    void givenValidInput_whenEnqueue_thenReturnEnqueuedElement() {
        Queue<Integer> dynamicQueue = new DynamicQueueImpl<>();
        int expected = 10;
        int actual = dynamicQueue.enqueue(10);

        assertEquals(expected, actual);
    }

    @Test
    void givenValidInput_whenPeek_thenReturnElementAtHead() {
        Queue<Integer> dynamicQueue = new DynamicQueueImpl<>();
        dynamicQueue.enqueue(1);
        dynamicQueue.enqueue(2);
        dynamicQueue.enqueue(3);

        assertEquals(1, dynamicQueue.peek());
    }

    @Test
    void givenEmptyQueue_whenPeek_thenReturnNull() {
        Queue<Integer> dynamicQueue = new DynamicQueueImpl<>();
        assertNull(dynamicQueue.peek());
    }

    @Test
    void givenValidQueue_whenDequeue_thenRemoveHeadElement() {
        Queue<Integer> dynamicQueue = new DynamicQueueImpl<>();
        dynamicQueue.enqueue(1);
        dynamicQueue.enqueue(2);
        dynamicQueue.enqueue(3);

        assertEquals(1, dynamicQueue.dequeue());
    }

    @Test
    void givenEmptyQueue_whenDeque_thenReturnNull(){
        Queue<Integer> dynamicQueue = new DynamicQueueImpl<>();
        assertNull(dynamicQueue.dequeue());
    }

    @Test
    void givenQueueWith3Elements_whenSize_thenReturn3(){
        Queue<Integer> dynamicQueue = new DynamicQueueImpl<>();
        dynamicQueue.enqueue(1);
        dynamicQueue.enqueue(2);
        dynamicQueue.enqueue(3);
        assertEquals(3, dynamicQueue.size());
    }
}