package org.progressoft.collection.stack;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

import java.util.EmptyStackException;

class DynamicStackImplTest {

    @Test
    void givenValidInput_whenPush_thenReturnPushedElement() {
        Stack<Integer> dyncamicStack = new DynamicStackImpl<>();
        int expected = 10;
        int actual = dyncamicStack.push(expected);
        assertEquals(expected, actual);
    }

    @Test
    void givenValidInput_whenPeek_thenReturnLastPushedElement() {
        Stack<Integer> dyncamicStack = new DynamicStackImpl<>();
        dyncamicStack.push(1);
        dyncamicStack.push(2);
        dyncamicStack.push(3);

        assertEquals(3, dyncamicStack.peek());
    }

    @Test
    void givenEmptyStack_whenPeek_thenThrowException(){
        Stack<Integer> dyncamicStack = new DynamicStackImpl<>();
        EmptyStackException emptyStackException = assertThrows(EmptyStackException.class, dyncamicStack::peek);
        assertNull(emptyStackException.getMessage());
    }

    @Test
    void givenValidInput_whenPop_thenReturnLastRemovedElement() {
        Stack<Integer> dyncamicStack = new DynamicStackImpl<>();
        dyncamicStack.push(1);
        dyncamicStack.push(2);
        dyncamicStack.push(3);

        assertEquals(3, dyncamicStack.pop());
    }

    @Test
    void givenEmptyStack_whenPop_thenThrowException(){
        Stack<Integer> dyncamicStack = new DynamicStackImpl<>();
        EmptyStackException emptyStackException = assertThrows(EmptyStackException.class, dyncamicStack::pop);
        assertNull(emptyStackException.getMessage());
    }

    @Test
    void givenStackWithSizeOf3_whenPushAndSize_thenReturn3() {
        Stack<Integer> dyncamicStack = new DynamicStackImpl<>();
        dyncamicStack.push(1);
        dyncamicStack.push(2);
        dyncamicStack.push(3);

        assertEquals(3, dyncamicStack.size());
    }

    @Test
    void givenStackWithSizeOf3_whenPopAndSize_thenReturn2() {
        Stack<Integer> dyncamicStack = new DynamicStackImpl<>();
        dyncamicStack.push(1);
        dyncamicStack.push(2);
        dyncamicStack.push(3);

        dyncamicStack.pop();

        assertEquals(2, dyncamicStack.size());
    }
}