package org.progressoft.collection.stack;

import org.junit.jupiter.api.Test;
import org.progressoft.collection.stack.Stack;
import org.progressoft.collection.stack.StaticStackImpl;

import java.util.EmptyStackException;

import static org.junit.jupiter.api.Assertions.*;

class StaticStackImplTest {

    @Test
    void givenValidInput_whenPush_thenReturnPushedElement() {
        Stack<Integer> staticStack = new StaticStackImpl<>();
        int expected = 7;
        int actual = staticStack.push(expected);
        assertEquals(expected, actual);
    }

    @Test
    void givenValidInputUntilOverflow_whenPush_thenThrowException() {
        Stack<Integer> staticStack = new StaticStackImpl<>(2);
        staticStack.push(1);
        staticStack.push(2);
        ArrayIndexOutOfBoundsException indexOutOfBoundsException =
                assertThrows(ArrayIndexOutOfBoundsException.class,
                        () -> staticStack.push(3));

        assertEquals("Array is full", indexOutOfBoundsException.getMessage());
    }

    @Test
    void whenNewStack_thenStackSizeShouldEqualZero(){
        Stack<Integer> staticStack = new StaticStackImpl<>();
        Stack<Integer> staticStackWithCapacity = new StaticStackImpl<>(2);
        assertEquals(staticStack.size(), 0);
        assertEquals(staticStackWithCapacity.size(), 0);
    }

    @Test
    void givenValidInput_whenPeek_thenReturnLastElementInStack(){
        Stack<String> staticStack = new StaticStackImpl<>();
        staticStack.push("Element 1");
        staticStack.push("Element 2");
        staticStack.push("Element 3");

        assertEquals("Element 3", staticStack.peek());
    }

    @Test
    void givenEmptyStack_whenPeek_thenThrowException(){
        Stack<String> staticStack = new StaticStackImpl<>();
        EmptyStackException stackException = assertThrows(EmptyStackException.class, staticStack::peek);
        assertNull(stackException.getMessage());
    }

    @Test
    void givenValidInput_whenPop_thenCheckIfElementIsRemoved(){
        Stack<String> staticStack = new StaticStackImpl<>();
        staticStack.push("Element 1");
        staticStack.push("Element 2");
        staticStack.push("Element 3");

        String removed = staticStack.pop();

        assertEquals("Element 3", removed);
        assertEquals("Element 2", staticStack.peek());

    }

    @Test
    void  givenEmptyStack_whenPop_thenThrowException(){
        Stack<String> staticStack = new StaticStackImpl<>();
        EmptyStackException emptyStackException = assertThrows(EmptyStackException.class, staticStack::pop);
        assertNull(emptyStackException.getMessage());
    }

}