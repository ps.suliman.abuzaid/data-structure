package org.progressoft.collection.queue;

public interface Queue<T> {
    T enqueue(T expected);
    T peek();

    T dequeue();

    int size();
}
