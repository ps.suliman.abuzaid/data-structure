package org.progressoft.collection.queue;

import org.progressoft.collection.util.NodeElement;

public class DynamicQueueImpl<T> implements Queue<T>{

    private NodeElement<T> head;
    private NodeElement<T> tail;
    private int size;
    @Override
    public T enqueue(T expected) {
        NodeElement<T> element = new NodeElement<>(expected);
        size++;
        if(head == null && tail == null){
            head = element;
            tail = head;
            return head.value();
        }

        tail.setNext(element);
        tail = tail.next();
        return tail.value();
    }

    @Override
    public T peek() {
        if(head == null){
            return null;
        }
        return head.value();
    }

    @Override
    public T dequeue() {
        if(head == null){
            return null;
        }
        size--;
        NodeElement<T> removedElement = head;
        head = head.next();
        return removedElement.value();
    }

    @Override
    public int size() {
        return size;
    }
}
