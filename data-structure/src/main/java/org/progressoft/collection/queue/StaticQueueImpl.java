package org.progressoft.collection.queue;

@SuppressWarnings("unchecked")
public class StaticQueueImpl<T> implements Queue<T>{

    private final Object[] elements;
    private int firstElmIdx;
    private int lastElmIdx;

    private int size;

    private static final int BASE_CAPACITY = 10;
    public StaticQueueImpl() {
        this.elements = new Object[BASE_CAPACITY];
    }

    public StaticQueueImpl(int capacity) {
        this.elements = new Object[capacity];
    }

    @Override
    public T enqueue(T expected) {
        if(lastElmIdx == elements.length && firstElmIdx > 0){
            lastElmIdx = 0;
        }else if(lastElmIdx == elements.length && firstElmIdx == 0) {
            throw new IllegalStateException("Queue is full");
        }
        size++;
        elements[lastElmIdx] = expected;
        return (T) elements[lastElmIdx++];
    }

    @Override
    public T peek() {
        return (T) elements[firstElmIdx];
    }

    @Override
    public T dequeue() {
        if(firstElmIdx == elements.length){
            firstElmIdx = 0;
        }
        size--;
        return (T)elements[firstElmIdx++];
    }

    @Override
    public int size() {
        return size;
    }
}
