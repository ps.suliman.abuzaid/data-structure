package org.progressoft.collection.stack;

import java.util.EmptyStackException;

@SuppressWarnings("unchecked")
public class StaticStackImpl<T> implements Stack<T> {

    private final Object[] elements;
    private static final int BASE_SIZE = 16;
    private int size;

    public StaticStackImpl() {
        elements = new Object[BASE_SIZE];
        size = 0;
    }

    public StaticStackImpl(int capacity) {
        elements = new Object[capacity];
        size = 0;
    }
    @Override
    public T push(T expected) {
        if(size  == elements.length){
            throw new ArrayIndexOutOfBoundsException("Array is full");
        }
        elements[size++] = expected;
        return (T) elements[size - 1];
    }

    @Override
    public T peek() {
        if(size == 0){
            throw new EmptyStackException();
        }
        return (T) elements[size-1];
    }

    @Override
    public T pop() {
        if(size == 0){
            throw new EmptyStackException();
        }
        Object poppedElement = elements[size - 1];
        elements[--size] = null;
        return (T) poppedElement;
    }
    @Override
    public int size(){
        return size;
    }

}
