package org.progressoft.collection.stack;

import org.progressoft.collection.util.NodeElement;

import java.util.EmptyStackException;

public class DynamicStackImpl<T> implements Stack<T>{

    private NodeElement<T> stack;
    private int size;

    @Override
    public T push(T expected) {
        NodeElement<T> nodeElement = new NodeElement<>(expected);
        size++;
        if(stack == null){
            stack = nodeElement;
            return stack.value();
        }
        NodeElement<T> temp = stack;

        stack.setNext(nodeElement);
        stack = stack.next();
        stack.setPrev(temp);
        return stack.value();
    }

    @Override
    public T peek() {
        isStackEmpty();
        return stack.value();
    }

    @Override
    public T pop() {
        isStackEmpty();
        size--;
        T removedValue = stack.value();
        stack = stack.prev();
        stack.setNext(null);
        return removedValue;
    }

    @Override
    public int size() {
        return size;
    }

    private void isStackEmpty() {
        if(stack == null){
            throw new EmptyStackException();
        }
    }
}
