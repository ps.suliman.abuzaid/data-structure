package org.progressoft.collection.stack;

public interface Stack<T> {
    T push(T expected);
    T peek();

    T pop();

    int size();
}
