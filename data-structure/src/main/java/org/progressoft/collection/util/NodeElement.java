package org.progressoft.collection.util;

public class NodeElement<T> {
    private T value;
    private NodeElement<T> next;
    private NodeElement<T> prev;

    public NodeElement(T value) {
        this.value = value;
    }

    public T value() {
        return value;
    }

    public NodeElement<T> next() {
        return next;
    }

    public NodeElement<T> prev() {
        return prev;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public void setNext(NodeElement<T> next) {
        this.next = next;
    }

    public void setPrev(NodeElement<T> prev) {
        this.prev = prev;
    }
}
