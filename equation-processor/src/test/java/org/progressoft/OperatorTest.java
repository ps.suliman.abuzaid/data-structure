package org.progressoft;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class OperatorTest {

    @Test
    void givenInput_whenPlus_thenReturnResult() {
        double expected = 10;
        double actual = Operator.plus.apply(5, 5);
        assertEquals(expected, actual);
    }

    @Test
    void givenInput_whenMinus_thenReturnResult() {
        double expected = 9;
        double actual = Operator.minus.apply(9, 18);
        assertEquals(expected, actual);
    }

    @Test
    void givenInput_whenMultiply_thenReturnResult() {
        double expected = 6;
        double actual = Operator.multiply.apply(2, 3);
        assertEquals(expected, actual);
    }

    @Test
    void givenInput_whenDivide_thenReturnResult() {
        double expected = 7;
        double actual = Operator.divide.apply(3, 21);
        assertEquals(expected, actual);
    }

    @Test
    void givenInput_whenBraceOpen_thenReturnResult() {
        double expected = 0;
        double actual = Operator.braceOpen.apply(2, 3);
        assertEquals(expected, actual);
    }

    @Test
    void givenInput_whenBraceClosed_thenReturnResult() {
        double expected = 0;
        double actual = Operator.braceClosed.apply(2, 3);
        assertEquals(expected, actual);
    }

    @Test
    void givenInput_whenExponent_thenReturnResult() {
        double expected = 8;
        double actual = Operator.exponent.apply(3, 2);
        assertEquals(expected, actual);
    }

    @Test
    void givenPlus_whenGetPriority_thenReturnResult() {
        double expected = 1;
        double actual = Operator.plus.getPriority();
        assertEquals(expected, actual);
    }

    @Test
    void givenBraceOpen_whenGetPriority_thenReturnResult() {
        double expected = 3;
        double actual = Operator.braceOpen.getPriority();
        assertEquals(expected, actual);
    }

    @Test
    void givenOperatorAsSymbolString_whenFrom_thenReturnResult() {
        Operator expected = Operator.multiply;
        Operator actual = Operator.from("*");
        assertEquals(expected, actual);
    }

    @Test
    void givenInvalidOperatorAsSymbolString_whenFrom_thenReturnNull() {
        Operator expected = null;
        Operator actual = Operator.from("~");
        assertEquals(expected, actual);
    }
}