package org.progressoft;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.InputMismatchException;

import static org.junit.jupiter.api.Assertions.*;

class EquationProcessorTest {

    @Test
    void givenInvalidEquationWithAdjacentOperators_whenProcess_thenThrowException(){
        IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class,
                ()->EquationProcessor.process("1 + * 2 - 6"));
        assertEquals("Invalid Equation", illegalArgumentException.getMessage());
    }

    @Test
    void givenValidSimpleEquation_whenProcess_thenReturnResult(){
        double expected = 9.0;
        double actual = EquationProcessor.process("9 / 3 - 4 + 2 * 5");
        assertEquals(expected, actual);
    }

    @Test
    void givenEquationWithVariablesAndValidInput_whenProcess_thenReturnResult(){
        String input = "4.0\n2.0";
        InputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);
        double expected = -2.0;
        double actual = EquationProcessor.process("x + 10 - 8 * y");
        assertEquals(expected, actual);
    }

    @Test
    void givenEquationWithBraces_whenProcess_thenReturnResult(){
        double expected = -16.0;
        double actual = EquationProcessor.process("2 * ( ( 6 / 2 - 4 ) * 9 ) + 2");
        assertEquals(expected, actual);
    }

    @Test
    void givenEquationWithVariablesAndInvalidInput_whenProcess_thenThrowException(){
        String input = "Hello\nWorld";
        InputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);
        assertThrows(InputMismatchException.class, ()-> EquationProcessor.process("x + 10 - 8 * y"));
    }

    @Test
    void givenEquationWithExponent_whenProcess_thenReturnResult(){
        double expected = 15.0;
        double actual = EquationProcessor.process("5 ^ 2 - 10 + ( 9 / 3 ^ 2 - 1 )");

        assertEquals(expected, actual);
    }

    @Test
    void givenInvalidEquation_whenProcess_thenThrowNumberFormatException(){
        IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class,() -> EquationProcessor.process("1* 2 - 3 + 7"));
        assertEquals("Invalid Equation", illegalArgumentException.getMessage());
    }

}