package org.progressoft;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Stack;
import java.util.function.Predicate;

public class EquationProcessor {
    // TODO: refactor
    public static double process(String equation) {
        Stack<Double> operands = new Stack<>();
        Stack<Operator> operations = new Stack<>();
        String[] equationElements = equation.split(" ");
        Map<String, Double> variableMap = new HashMap<>();
        Scanner scanner = new Scanner(System.in);

        String currentElement;
        boolean isElementNumeric;
        boolean isElementVariable;
        Operator currentOperator;

        for (int equationIdx = 0; equationIdx < equationElements.length; ) {
            currentElement = equationElements[equationIdx];
            isElementNumeric = isNumeric(currentElement);
            isElementVariable = isVariable(currentElement);

            if (isElementNumeric) {
                pushOperand(operands, currentElement);
                equationIdx++;
                continue;
            }

            if (isElementVariable && variableMap.get(currentElement) != null) {
                pushVariable(operands, variableMap, currentElement);
                equationIdx++;
                continue;
            }

            currentOperator = Operator.from(currentElement);

            if (currentOperator == null) {
                promptVariable(isElementVariable, currentElement, variableMap, scanner);
                continue;
            }

            if (operations.isEmpty()) {
                pushOperator(operations, currentOperator);
                equationIdx++;
                continue;
            }

            if (!operations.peek().equals(Operator.braceOpen) && operations.peek().getPriority() >= currentOperator.getPriority()) {
                if (operands.size() < 2)
                    throw new IllegalArgumentException("Invalid Equation");
                if (!calculateBracesResult(operations, operands))
                    calculateResult(operands, operations);
                continue;
            }

            pushOperator(operations, currentOperator);
            equationIdx++;
        }

        calculateFinalResult(operands, operations);

        return operands.peek();
    }

    private static void pushVariable(Stack<Double> operands, Map<String, Double> variableMap, String currentElement) {
        operands.push(variableMap.get(currentElement));
    }

    private static void calculateFinalResult(Stack<Double> operands, Stack<Operator> operations) {
        while (operands.size() > 1) {
            calculateResult(operands, operations);
        }
    }

    private static void pushOperator(Stack<Operator> operations, Operator operator) {
        operations.push(operator);
    }

    private static void promptVariable(boolean isElementVariable, String currentElement, Map<String, Double> variableMap, Scanner scanner) {
        if (!isElementVariable) {
            throw new IllegalArgumentException("Invalid Equation");
        }
        System.out.println("Please Enter a number for variable " + currentElement + ": ");
        variableMap.put(currentElement, scanner.nextDouble());
        return;
    }

    private static void pushOperand(Stack<Double> operands, String currentElement) {
        try {
            operands.push(Double.parseDouble(currentElement));
        } catch (NumberFormatException numberFormatException) {
            throw new NumberFormatException("Could not parse string to number");
        }
    }

    private static boolean calculateBracesResult(Stack<Operator> operations, Stack<Double> operands) {
        if (operations.peek().equals(Operator.braceClosed)) {
            operations.pop();
            while (!operations.peek().equals(Operator.braceOpen)) {
                calculateResult(operands, operations);
            }
            operations.pop();
            return true;
        }
        return false;
    }

    private static void calculateResult(Stack<Double> operands, Stack<Operator> operations) {
        double result;
        double number1 = operands.pop();
        double number2 = operands.pop();
        result = operations.pop().apply(number1, number2);
        operands.push(result);
    }

    private static boolean isNumeric(String str) {
        for (int index = 0; index < str.length(); index++) {
            if (!Character.isDigit(str.charAt(index))) {
                return false;
            }
        }
        return true;
    }

    private static boolean isVariable(String str) {
        if (!str.isEmpty() && !Character.isLetter(str.charAt(0)) && Character.isDigit(str.charAt(0))) {
            return false;
        }
        char character;
        for (int index = 1; index < str.length(); index++) {
            character = str.charAt(index);
            if (!Character.isLetter(character) && !Character.isDigit(character)) {
                return false;
            }
        }
        return true;
    }


}
