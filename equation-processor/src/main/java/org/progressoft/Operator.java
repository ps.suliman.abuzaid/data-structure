package org.progressoft;

import java.util.Arrays;

public enum Operator {
    plus("+", 1) {
        @Override
        public double apply(double number1, double number2) {
            return number2 + number1;
        }
    },
    minus("-", 1) {
        @Override
        public double apply(double number1, double number2) {
            return number2 - number1;
        }
    },
    multiply("*", 2){
        @Override
        public double apply(double number1, double number2) {
            return number2 * number1;
        }
    },
    divide("/", 2){
        @Override
        public double apply(double number1, double number2) {
            return number2 / number1;
        }
    },
    braceOpen("(", 3){
        @Override
        public double apply(double number1, double number2){
            return 0;
        }
    },
    braceClosed(")", 3){
        @Override
        public double apply(double number1, double number2){
            return 0;
        }
    },
    exponent("^", 4){
        @Override
        public double apply(double number1, double number2) {
            return Math.pow(number2, number1);
        }
    };

    private final String operatorString;
    private final int priority;

    Operator(String operatorString, int priority) {
        this.operatorString = operatorString;
        this.priority = priority;
    }


    public int getPriority() {
        return priority;
    }

    public abstract double apply(double number1, double number2);

    public static Operator from(String str) {
        return Arrays.stream(values()).filter(o -> o.operatorString.equals(str)).findFirst()
                .orElse(null);
    }
}