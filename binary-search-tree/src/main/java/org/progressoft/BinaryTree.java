package org.progressoft;

public class BinaryTree {
    private Node root;

    public boolean accept(int data) {
        if (root == null) {
            root = new Node(data);
            return true;
        }
        return root.accept(data);
    }

    public Node root() {
        return root;
    }

    public void setRoot(Node node) {
        this.root = node;
    }

    public int findDepth(int value){
        return findDepth(root(), value);
    }

    private int findDepth(Node node, int value) {
        int depth;
        if(node == null)
            return -1;
        if(node.data() == value)
            return 0;
        else if(node.data() > value) {
            depth = findDepth(node.left(), value);
            return depth > -1 ? depth + 1 : depth;
        }
        depth = findDepth(node.right(), value);
        return  depth > -1 ? depth + 1: depth;
    }

    public int treeDepth() {
        if(root == null){
            return -1;
        }
        return treeDepth(root);
    }


    private int treeDepth(Node node){
        int leftDepth = 0;
        int rightDepth = 0;
        if(node.left() == null && node.right() == null){
            return 0;
        }

        if(node.left() != null){
            leftDepth = treeDepth(node.left());
        }

        if(node.right() != null){
            rightDepth = treeDepth((node.right()));
        }

        return leftDepth > rightDepth ? leftDepth + 1: rightDepth + 1;

    }
}
