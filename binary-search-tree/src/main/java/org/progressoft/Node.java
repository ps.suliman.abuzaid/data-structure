package org.progressoft;

public class Node {

    private int data;

    private Node left;

    private Node right;
    public Node(int data) {
        this.data = data;
    }

    public boolean accept(int value) {
        if(data == value){
            return false;
        }

        if(value > data){
            if(right != null) return right.accept(value);
            right = new Node(value);
            return true;
        }

        if (left != null) return left.accept(value);
        left = new Node(value);
        return true;
    }

    public int data() {
        return data;
    }

    public Node left() {
        return left;
    }

    public void setLeft(Node left) {
        this.left = left;
    }

    public Node right() {
        return right;
    }

    public void setRight(Node right) {
        this.right = right;
    }
}
