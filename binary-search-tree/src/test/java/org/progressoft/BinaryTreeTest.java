package org.progressoft;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BinaryTreeTest {

    @Test
    void givenInputWithNoRootNode_whenAccept_thenCreateRootNodeAndReturnTrue(){
        BinaryTree binaryTree = new BinaryTree();
        assertTrue(binaryTree.accept(5));
        assertNotNull(binaryTree.root());
    }

    @Test
    void givenInputGreaterThanRootNodeData_whenAccept_thenAppendNewNodeToTheRightOfRootNodeAndReturnTrue(){
        BinaryTree binaryTree = new BinaryTree();
        binaryTree.setRoot(new Node(5));
        assertTrue(binaryTree.accept(6));
        assertNotNull(binaryTree.root().right());
        assertEquals(binaryTree.root().right().data(), 6);
    }
    @Test
    void givenInputLessThanRootNodeData_whenAccept_thenAppendNewNodeToTheLessOfRootNodeAndReturnTrue(){
        BinaryTree binaryTree = new BinaryTree();
        binaryTree.setRoot(new Node(5));
        assertTrue(binaryTree.accept(4));
        assertNotNull(binaryTree.root().left());
        assertEquals(binaryTree.root().left().data(), 4);
    }

    @Test
    void givenInputEqualToRootNodeData_whenAccept_thenReturnFalse(){
        BinaryTree binaryTree = new BinaryTree();
        binaryTree.setRoot(new Node(5));
        assertFalse(binaryTree.accept(5));
        assertNull(binaryTree.root().left());
        assertNull(binaryTree.root().right());
    }

    @Test
    void givenInputToEmptyTree_whenFindDepth_thenReturnMinusOne(){
        BinaryTree binaryTree = new BinaryTree();
        int expectedDepthOf10 = -1;
        int actualDepthOf10 = binaryTree.findDepth(10);
        assertEquals(expectedDepthOf10, actualDepthOf10);
    }

    @Test
    void givenInputEqualToRoot_whenFindDepth_thenReturnDepth(){
        BinaryTree binaryTree = new BinaryTree();
        binaryTree.setRoot(new Node(5));
        binaryTree.accept(4);
        binaryTree.accept(3);
        binaryTree.accept(6);
        binaryTree.accept(7);

        int expectedDepthOf5 = 0;
        int actualDepthOf5 = binaryTree.findDepth(5);
        assertEquals(expectedDepthOf5, actualDepthOf5);
    }

    @Test
    void givenInputLessThanRoot_whenFindDepth_thenReturnDepth(){
        BinaryTree binaryTree = new BinaryTree();
        binaryTree.setRoot(new Node(5));
        binaryTree.accept(4);
        binaryTree.accept(3);
        binaryTree.accept(6);
        binaryTree.accept(7);

        int expectedDepthOf3 = 2;
        int actualDepthOf3 = binaryTree.findDepth(3);
        assertEquals(expectedDepthOf3, actualDepthOf3);
    }

    @Test
    void givenInputGreaterThanRoot_whenFindDepth_thenReturnDepth(){
        BinaryTree binaryTree = new BinaryTree();
        binaryTree.setRoot(new Node(5));
        binaryTree.accept(4);
        binaryTree.accept(3);
        binaryTree.accept(7);
        binaryTree.accept(6);
        binaryTree.accept(8);

        int expectedDepthOf8 = 2;
        int actualDepthOf8 = binaryTree.findDepth(8);
        assertEquals(expectedDepthOf8, actualDepthOf8);
    }

    @Test
    void givenInputNotInBinaryTree_whenFindDepth_thenReturnMinusOne(){
        BinaryTree binaryTree = new BinaryTree();
        binaryTree.setRoot(new Node(5));
        binaryTree.accept(4);
        binaryTree.accept(3);
        binaryTree.accept(7);
        binaryTree.accept(6);
        binaryTree.accept(8);

        int expectedDepthOf99 = -1;
        int actualDepthOf99 = binaryTree.findDepth(99);
        assertEquals(expectedDepthOf99, actualDepthOf99);
    }

    @Test
    void givenEmptyTree_whenTreeDepth_thenReturnMinusOne(){
        BinaryTree binaryTree = new BinaryTree();

        int expectedTreeDepth = -1;
        int actualTreeDepth = binaryTree.treeDepth();
        assertEquals(expectedTreeDepth, actualTreeDepth);
    }

    @Test
    void givenBinaryTree_whenTreeDepth_thenReturnTreeDepth(){
        BinaryTree binaryTree = new BinaryTree();
        binaryTree.setRoot(new Node(10));
        binaryTree.accept(5);
        binaryTree.accept(7);
        binaryTree.accept(6);
        binaryTree.accept(8);
        binaryTree.accept(3);
        binaryTree.accept(4);
        binaryTree.accept(2);
        binaryTree.accept(1);
        binaryTree.accept(14);
        binaryTree.accept(12);
        binaryTree.accept(13);
        binaryTree.accept(11);
        binaryTree.accept(16);
        binaryTree.accept(15);
        binaryTree.accept(17);
        binaryTree.accept(18);


        int expectedTreeDepth = 4;
        int actualTreeDepth = binaryTree.treeDepth();

        assertEquals(expectedTreeDepth, actualTreeDepth);
    }

}