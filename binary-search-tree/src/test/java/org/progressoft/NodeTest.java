package org.progressoft;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;


public class NodeTest{

    @Test
    void givenInputEqualToNodeValue_whenAccept_thenReturnFalse(){
        Node node = new Node(5);
        assertFalse(node.accept(5));
    }

    @Test
    void givenInputGreaterThanNodeValueAndRightNodeIsNull_whenAccept_thenCreateRightNodeAndReturnTrue(){
        Node node = new Node(5);
        assertTrue(node.accept(6));
        assertNotNull(node.right());
        assertEquals(node.right().data(), 6);
    }

    @Test
    void givenInputGreaterThanNodeValueAndEqualRightNodeValue_whenAccept_thenPassTheValueToRightNodeAcceptAndReturnResult(){
        Node node = new Node(5);
        node.setRight(new Node(6));
        assertFalse(node.accept(6));
    }

    @Test
    void givenInputGreaterThanNodeValueAndGreaterThanRightNodeValue_whenAccept_thenPassTheValueToRightNodeAcceptAndReturnResult(){
        Node node = new Node(5);
        node.setRight(new Node(6));
        assertTrue(node.accept(7));
        assertNotNull(node.right().right());
        assertEquals(node.right().right().data(), 7);
    }

    @Test
    void givenInputLessThanNodeValueAndLeftNodeIsNull_whenAccept_thenCreateLeftNodeAndReturnTrue(){
        Node node = new Node(5);
        assertTrue(node.accept(4));
        assertNotNull(node.left());
    }

    @Test
    void givenInputLessThanNodeValueAndEqualLeftNodeValue_whenAccept_thenPassTheValueToLeftNodeAcceptAndReturnResult(){
        Node node = new Node(5);
        node.setLeft(new Node(4));
        assertFalse(node.accept(4));
    }

    @Test
    void givenInputLessThanNodeValueAndLessThanLeftNodeValue_whenAccept_thenPassTheValueToLeftNodeAcceptAndReturnResult(){
        Node node = new Node(5);
        node.setLeft(new Node(4));
        assertTrue(node.accept(3));
        assertEquals(node.left().left().data(), 3);
    }



}