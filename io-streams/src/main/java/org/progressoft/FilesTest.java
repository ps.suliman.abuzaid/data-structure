package org.progressoft;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class FilesTest {

    public static void main(String[] args) throws IOException {
        Path path = Paths.get("./io-streams",  "dir1");
        Path textFile = path.resolve("Sample.txt");
        if(Files.notExists(textFile))
            Files.createDirectory(path);
        BufferedWriter writer = Files.newBufferedWriter(textFile, StandardOpenOption.APPEND);
        try (writer) {
            PrintWriter printWriter = new PrintWriter(writer);
            printWriter.println("Hello World");
        }
    }
}
