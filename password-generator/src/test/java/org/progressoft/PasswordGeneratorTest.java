package org.progressoft;

import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PasswordGeneratorTest {

    @Test
    void whenGenerate_thenCheckPasswordNotNull() {
        String password = PasswordGenerator.generate();
        assertNotNull(password, "Password should not be null");
    }

    @Test
    void whenGenerate_thenCheckPasswordLength() {
        int expected = 8;
        String password = PasswordGenerator.generate();
        int actual = password.length();
        assertEquals(expected, actual);
    }

    @Test
    void whenGenerate_thenCheckIfSubSequentPasswordsAreNotEqual() {
        for (int index = 0; index < 10; index++) {
            String password1 = PasswordGenerator.generate();
            String password2 = PasswordGenerator.generate();
            assertNotEquals(password1, password2);
        }
    }

    @RepeatedTest(100)
    void whenGenerate_thenCheckIfSubSequentPasswordsAreNotEqualToTheFirstGeneratedPassword() {
        String password1 = PasswordGenerator.generate();
        String password2 = PasswordGenerator.generate();
        assertNotEquals(password1, password2);
    }

    @Test
    void whenGenerate_thenCheckIfPasswordHasFourLetters() {
        String password = PasswordGenerator.generate();
        int expectedLetterCount = 2;
        int actualLetterCount = 0;
        int passwordLength = password.length();
        char character;
        for (int index = 0; index < passwordLength; index++) {
            character = password.charAt(index);
            if (Character.isAlphabetic(character)) {
                actualLetterCount++;
            }
        }
        assertEquals(expectedLetterCount, actualLetterCount);
    }

    @Test
    void whenGenerate_thenCheckIfPasswordHasTwoNumbers() {
        String password = PasswordGenerator.generate();
        int expectedNumberCount = 4;
        int actualNumberCount = 0;
        int passwordLength = password.length();
        char character;
        for (int index = 0; index < passwordLength; index++) {
            character = password.charAt(index);
            if (Character.isDigit(character)) {
                actualNumberCount++;
            }
        }
        assertEquals(expectedNumberCount, actualNumberCount);
    }

    @Test
    void whenGenerate_thenCheckIfPasswordHasTwoSymbols() {
        String password = PasswordGenerator.generate();
        int expectedSymbolsCount = 2;
        int actualSymbolsCount = 0;
        int passwordLength = password.length();
        char character;
        for (int index = 0; index < passwordLength; index++) {
            character = password.charAt(index);
            if (!Character.isAlphabetic(character) && !Character.isDigit(character)) {
                actualSymbolsCount++;
            }
        }
        assertEquals(expectedSymbolsCount, actualSymbolsCount);
    }

}