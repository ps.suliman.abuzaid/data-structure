package org.progressoft;

import java.util.Random;

public class PasswordGenerator {
    private static final String salt = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_$#%";

    public static String generate() {
        int index, lettersLimit = 2, numbersLimit = 4, symbolsLimit = 2;
        char character;
        boolean isDigit, isAlpha;
        StringBuilder passwordToGenerate = new StringBuilder();
        Random random = new Random();
        while (passwordToGenerate.length() < 8) {
            index = random.nextInt( salt.length());
            character = salt.charAt(index);
            isDigit = Character.isDigit(character);
            isAlpha = Character.isAlphabetic(character);

            if (isDigit && numbersLimit > 0) {
                passwordToGenerate.append(character);
                numbersLimit--;
                continue;
            }

            if (isAlpha && lettersLimit > 0) {
                passwordToGenerate.append(character);
                lettersLimit--;
                continue;
            }

            if (!isAlpha && !isDigit && symbolsLimit > 0) {
                passwordToGenerate.append(character);
                symbolsLimit--;
            }
        }
        return passwordToGenerate.toString();
    }
}
